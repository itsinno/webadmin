/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

const {
	ESLINT_MODES
	// getLoader,
	// loaderByName,
	// throwUnexpectedConfigError
} = require('@craco/craco');

// const CracoLessPlugin = require('craco-less');
//
// const path = require('path');
//
// const SUI_LESS_PLUGIN = {
// 	overrideWebpackConfig: ({ context, webpackConfig }) => {
// 		webpackConfig.resolve.alias['../../theme.config$'] =
// 			path.join(context.paths.appSrc, '/semantic-ui/theme.config');
//
// 		const {
// 			isFound,
// 			match: fileLoaderMatch
// 		} = getLoader(webpackConfig, loaderByName('file-loader'));
//
// 		if (!isFound) {
// 			throwUnexpectedConfigError({
// 				packageName: '@semantic-ui-react/craco-less',
// 				message: `Can't find "file-loader" in the ${context.env} webpack config!`
// 			});
// 		}
//
// 		fileLoaderMatch.loader.exclude.push(/theme.config$/);
// 		fileLoaderMatch.loader.exclude.push(/\.variables$/);
// 		fileLoaderMatch.loader.exclude.push(/\.overrides$/);
//
// 		return CracoLessPlugin.overrideWebpackConfig({
// 			context,
// 			webpackConfig
// 		});
// 	}
// };
//
// const PLUGINS = [{ plugin: SUI_LESS_PLUGIN }];

module.exports = {
	// plugins: PLUGINS,
	eslint: {
		mode: ESLINT_MODES.file
	},
	webpack: {}
};

