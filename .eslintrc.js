/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

module.exports = {
	extends: ['eslint-config-react-app'],
	rules: {
		'quotes': [
			1,
			'single',
			{
				avoidEscape: true,
				allowTemplateLiterals: true
			}
		],
		'no-await-in-loop': 'warn',
		'no-extra-parens': 'warn',
		'camelcase': 'warn',
		'brace-style': 'warn',
		'indent': ['warn', 'tab'],
		'jsx-quotes': ['warn', 'prefer-single']
	}
};
