/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                             from 'lodash';
import React, { Fragment }           from 'react';
import { withTranslation }           from 'react-i18next';
import { connect }                   from 'react-redux';
import { withRouter }                from 'react-router-dom';
import { Grid, GridColumn, GridRow } from 'semantic-ui-react';
import ApiResolver                   from '../api/ApiResolver';
import User                          from '../api/auth/User';
import BlockType                     from '../components/statistics/BlockType';
import ControlledStatistic           from '../components/statistics/ControlledStatistic';
import RatingBlock                   from '../components/statistics/RatingBlock';
import StatisticBlock                from '../components/statistics/StatisticBlock';
import StatisticHeader               from '../components/statistics/StatisticHeader';
import { redux }                     from '../utils/misc';

const ENGINEERS_TOP_SIZE = 3;
const CATEGORY_TOP_SIZE = 5;
const STATISTICS_INVERTED = false;

class Statistics extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: {},
			stats: {
				kbArticles: 0,
				tickets: {
					current: {
						open: 0,
						processing: 0,
						resolved: 0
					}
				},
				bots: {
					total: 0,
					alive: 0,
					dead: 0,
					acceptingMessages: 0,
					denyingMessages: 0
				},
				engineers: []
			},
			period: 'week'
		};
	}

	get t() {
		return this.props.t;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: toast => this.props.openToast(toast),
			closeToast: toastId => this.props.closeToast(toastId)
		};
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster);
	}

	get location() {
		return this.props.location || {};
	}

	get calls() {
		return {
			'kb.articles': async () => await this.countKBArticles(),
			bots: async () => await this.getBotsStatus(),
			'tickets.current': async () => await this.getTicketsStatus(),
			'tickets.stats': async () => await this.getTicketStats(),
			'tickets.categoryTop': async () => await this.getCategoryTop(),
			engineers: async () => await this.getEngineerTop()
		};
	}

	render() {
		return (
			<Fragment>
				<Grid>
					<GridRow>
						<GridColumn>
							<StatisticHeader
								inverted={STATISTICS_INVERTED}
								period={this.state.period}
								onChange={async value => await this.setPeriod(value)}
							/>
						</GridColumn>
					</GridRow>
					<GridRow>
						<GridColumn width={5}>
							<StatisticBlock
								widths={2}
								title={this.t('title') /*"General"*/}
								inverted={STATISTICS_INVERTED}
							>
								<ControlledStatistic
									label={this.t('stat.articles') /*"Articles"*/}
									loading={this.getStatLoading('kb.articles')}
									value={this.getStatValue('kb.articles')}
								/>
								<ControlledStatistic
									label={this.t('stat.open-tickets') /*"Open tickets"*/}
									loading={this.getStatLoading('tickets.current')}
									value={this.getStatValue('tickets.current.open')}
								/>
								<ControlledStatistic
									label={this.t('stat.pending-tickets') /*"Pending tickets"*/}
									loading={this.getStatLoading('tickets.current')}
									value={this.getStatValue('tickets.current.processing')}
								/>
								<ControlledStatistic
									label={this.t('stat.resolved-tickets') /*"Resolved tickets"*/}
									loading={this.getStatLoading('tickets.current')}
									value={this.getStatValue('tickets.current.resolved')}
								/>
							</StatisticBlock>
						</GridColumn>
						<GridColumn width={6}>
							<StatisticBlock
								widths={2}
								title={this.t('tickets') /*"Tickets"*/}
								type={BlockType.STATISTIC}
								inverted={STATISTICS_INVERTED}
							>
								<ControlledStatistic
									label={this.t('ticket.created') /*"created"*/}
									loading={this.getStatLoading('tickets.stats')}
									value={this.getStatValue('tickets.stats.created')}
								/>
								<ControlledStatistic
									label={this.t('ticket.resolved') /*"resolved"*/}
									loading={this.getStatLoading('tickets.stats')}
									value={this.getStatValue('tickets.stats.resolved')}
								/>
								<ControlledStatistic
									label={
										this.t('ticket.avg-response-time') /*"AVG response time"*/
									}
									loading={this.getStatLoading('tickets.stats')}
									value={this.getStatValue('tickets.stats.responseTime')}
								/>
								<ControlledStatistic
									label={
										this.t(
											'ticket.avg-resolution-time'
										) /*"AVG resolution time"*/
									}
									loading={this.getStatLoading('tickets.stats')}
									value={this.getStatValue('tickets.stats.resolveTime')}
								/>
							</StatisticBlock>
						</GridColumn>
						<GridColumn width={5}>
							<StatisticBlock
								widths={2}
								title={this.t('bots') /*"Bots"*/}
								inverted={STATISTICS_INVERTED}
							>
								<ControlledStatistic
									label={this.t('bot.alive') /*"alive"*/}
									loading={this.getStatLoading('bots')}
									value={this.getStatValue('bots.acceptingMessages')}
								/>
								<ControlledStatistic
									label={this.t('bot.dead') /*"dead"*/}
									loading={this.getStatLoading('bots')}
									value={this.getStatValue('bots.denyingMessages')}
								/>
							</StatisticBlock>
						</GridColumn>
					</GridRow>
					<GridRow>
						<GridColumn width={8}>
							<RatingBlock
								keyName={this.t('rating.category') /*"Category"*/}
								valueName={this.t('rating.tickets') /*"Tickets"*/}
								title={this.t('rating.top-categories') /*"Top categories"*/}
								count={CATEGORY_TOP_SIZE}
								type={BlockType.STATISTIC}
								inverted={STATISTICS_INVERTED}
								data={this.getStatValue('tickets.categoryTop')}
								loading={this.getStatLoading('tickets.categoryTop')}
							/>
						</GridColumn>
						<GridColumn width={8}>
							<RatingBlock
								keyName={this.t('rating.engineer') /*"Engineer"*/}
								valueName={this.t('rating.resolved') /*"Resolved"*/}
								title={this.t('rating.top-engineers') /*"Top engineers"*/}
								count={ENGINEERS_TOP_SIZE}
								type={BlockType.STATISTIC}
								inverted={STATISTICS_INVERTED}
								data={this.getStatValue('engineers')}
								loading={this.getStatLoading('engineers')}
							/>
						</GridColumn>
					</GridRow>
				</Grid>
			</Fragment>
		);
	}

	async componentDidMount() {
		await this.updateAll();
	}

	async countKBArticles() {
		return await this.api.knowledgeBase.countArticles();
	}

	async getBotsStatus() {
		return await this.api.stats.botsStatus();
	}

	async getTicketsStatus() {
		return await this.api.stats.ticketsStatus();
	}

	async getTicketStats() {
		return await this.api.stats.ticketStats({ period: this.state.period });
	}

	getStatValue = name => _.get(this.state.stats, name, 0);
	getStatLoading = name => _.get(this.state.loading, name);

	async setStatLoading(name, loading) {
		_.set(this.state.loading, name, loading);
		await this.setState({});
	}

	async setStatValue(name, value) {
		await this.setStatLoading(name, true);

		_.set(this.state.stats, name, value);
		await this.setState({});

		await this.setStatLoading(name, false);
	}

	async getEngineerTop() {
		return await this.api.stats.engineersTop({
			count: ENGINEERS_TOP_SIZE,
			period: this.state.period
		});
	}

	async getCategoryTop() {
		return await this.api.stats.ticketCategoriesTop({
			count: CATEGORY_TOP_SIZE,
			period: this.state.period
		});
	}

	async setPeriod(period) {
		await this.setState({ period });

		await this.updateAll();
	}

	async updateAll() {
		await Promise.all(
			_.keys(this.calls).map(async key => {
				await this.setStatLoading(key, true);
				let value = await this.calls[key]();
				await this.setStatValue(key, value);
				await this.setStatLoading(key, false);
			})
		);
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withRouter(withTranslation('Statistics')(Statistics)));
