/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                                                           from 'lodash';
import React                                                       from 'react';
import { withTranslation }                                         from 'react-i18next';
import { connect }                                                 from 'react-redux';
import { Container, Divider, Header, Icon, Segment, SegmentGroup } from 'semantic-ui-react';
import ApiResolver                                                 from '../../api/ApiResolver';
import User                                                        from '../../api/auth/User';
import DateTag                                                     from '../../components/commons/DateTag';
import Tags                                                        from '../../components/knowledge-base/commons/Tags';
import FlexCol                                                     from '../../utils/components/flex/FlexCol';
import FlexRow                                                     from '../../utils/components/flex/FlexRow';
import { redux }                                                   from '../../utils/misc';

class Article extends React.Component {
	static renderTags(tags) {
		return <Tags tags={tags}/>;
	}

	static renderLoadingState() {
		return <Segment loading padded={'very'}>
			<Divider clearing hidden/>
			<Divider clearing hidden/>
			<Divider clearing hidden/>
			<Divider clearing hidden/>
		</Segment>;
	}

	constructor(props) {
		super(props);
		this.state = {
			loading: true
		};
	}

	get t() {
		return this.props.t;
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast
		};
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).knowledgeBase;
	}

	async loading(loading) {
		await this.setState({ loading });
	}

	async componentDidMount() {
		await this.loading(true);
		await this.loadArticle();
		await this.loading(false);
	}

	async loadArticle() {
		let article = await this.api.getArticle(this.props);
		await this.setState({ article });
	}

	render() {
		return <Container>
			<SegmentGroup raised color={'green'}>
				{this.state.loading ? Article.renderLoadingState() : this.renderBody()}
			</SegmentGroup>
		</Container>;
	}

	renderBody() {
		return this.state.article ?
			this.renderArticle() :
			this.renderNotFound();
	}

	renderArticle() {
		return <>
			<Segment padded inverted color={'green'}>
				<Header icon={'book'} content={this.state.article.title}/>
			</Segment>
			<Segment padded>
				<FlexRow>
					<FlexCol style={{ margin: 0 }} align={'center'} grow={1}>
						{Article.renderTags(this.state.article.tags)}
					</FlexCol>
					<FlexCol style={{ margin: 0 }} align={'center'} grow={0}>
						<DateTag date={this.state.article.published}/>
					</FlexCol>
				</FlexRow>
			</Segment>
			<Segment padded>
				<Container fluid>
				<div dangerouslySetInnerHTML={{__html: this.state.article.content}} />
				</Container>
			</Segment>
		</>;
	}

	// noinspection JSMethodCanBeStatic
	renderNotFound() {
		return <Segment placeholder>
			<Header as={'h2'} icon>
				<Icon name={'find'}/>
				{this.t('article-not-found')/*Article was not found*/}
			</Header>
		</Segment>;
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('KnowledgeBase/Article')(Article));
