import React, { Fragment } from 'react';
import { Divider, Header } from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

// noinspection JSUnusedGlobalSymbols
class UserPreferences extends React.Component {
	get t() {
		return this.props.t;
	}

	render() {
		return (
			<Fragment>
				<Header>{this.t('title') /*User Preferences*/}</Header>
				<Divider />
			</Fragment>
		);
	}
}

export default withTranslation('UserPreferences')(UserPreferences);
