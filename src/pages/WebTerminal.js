/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                                             from 'lodash';
import React, { Fragment }                           from 'react';
import { Dimmer, Menu, MenuItem, MenuMenu, Segment } from 'semantic-ui-react';
import { defaultWTPlugin }                           from '../utils/terminal-plugins/WebTerminalPlugin';
import Terminal                                      from 'terminal-in-react';
import TemplateList                                  from '../components/web-terminal/TemplateList';
import ScriptList                                    from '../components/web-terminal/ScriptList';
import Config                                        from '../config/base';
import { buildLocation, nullify, redux }             from '../utils/misc';
import { connect }                                   from 'react-redux';
import User                                          from '../api/auth/User';
import { withRouter }                                from 'react-router-dom';
import Authority                                     from '../api/auth/Authority';
import { MT_FAILURE }                                from '../utils/components/toaster/types';
import Toast                                         from '../utils/components/toaster/Toast';
import ApiResolver                                   from '../api/ApiResolver';
import ScriptParser                                  from '../utils/web-terminal/ScriptParser';
import { withTranslation }                           from 'react-i18next';

const TERMINAL_THEME = Config.webTerminal.terminal.theme;
const TERMINAL_SETTINGS = Config.webTerminal.terminal.settings;
const WEB_TERMINAL_AUTHORITY = Authority.admin;

class WebTerminal extends React.Component {
	constructor(props) {
		super(props);

		this.state = {};
		this.executor = defaultWTPlugin();
	}

	get t() {
		return this.props.t;
	}

	get plugins() {
		return [this.executor.class];
	}

	get denied() {
		return Authority.of(this.user).denied(WEB_TERMINAL_AUTHORITY);
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get anonymous() {
		return this.user.isAnonymous();
	}

	get here() {
		return buildLocation(this.props.location || {});
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).webTerminal;
	}

	get toaster() {
		return {
			openToast: toast => this.props.openToast(toast),
			closeToast: toastId => this.props.closeToast(toastId)
		};
	}

	async goBack() {
		if (_.isString(this.state.toastId))
			await this.props.closeToast({ id: this.state.toastId });
		await this.props.history.goBack();
	}

	async componentDidMount() {
		await this.executor.instance.update();
		await this.executor.instance.printMessage(
			this.t(
				'warning'
			) /*'Warning! Web Terminal is experimental, 
		some features may not work as expected!'*/
		);
		if (this.denied && !this.anonymous) {
			// noinspection HtmlUnknownAnchorTarget
			let toast = Toast.withId({
				type: MT_FAILURE,
				header: this.t('access-denied') /*'Access denied!'*/,
				message:
					<Fragment>
						{this.t(
							'you-have-no-access'
						) /*You don't have an access to this page!*/}
						<br/>
						<a href='#go-back' onClick={async () => await this.goBack()}>
							{this.t('go-back') /*Go back*/}
						</a>
					</Fragment>

			});
			await this.setState({ toastId: toast.id });
			await this.props.openToast(toast);
		}
	}

	async componentWillUnmount() {
		if (_.isString(this.state.toastId))
			await this.props.closeToast({ id: this.state.toastId });
	}

	render() {
		return (
			<Fragment>
				<Segment inverted raised className={'no-padding'}>
					<Segment
						inverted
						size={'big'}
						attached={'top'}
						className={'terminal-header'}
					>
						{this.t('web-terminal') /*Web terminal*/}
					</Segment>
					<Menu
						attached
						icon={'labeled'}
						size={'small'}
						inverted
						className={'menu-embedded'}
					>
						<MenuMenu position={'left'}>
							<TemplateList
								onExecute={async command =>
									await this.executor.instance.executeCommand(command)
								}
								trigger={
									<MenuItem
										icon={'terminal'}
										color={'teal'}
										floated={'right'}
										content={this.t('script-templates') /*'Script templates'*/}
									/>
								}
							/>
							<ScriptList
								trigger={
									<MenuItem
										icon={'code'}
										color={'teal'}
										floated={'right'}
										content={this.t('scripts') /*'Scripts'*/}
									/>
								}
							/>
						</MenuMenu>
					</Menu>
					<Terminal
						{...TERMINAL_THEME}
						{...TERMINAL_SETTINGS}
						plugins={this.plugins}
						commands={{
							echo: (args, cb) => nullify(this.onCommand(args, cb))
						}}
						commandPassThrough={(command, callback) =>
							nullify(this.onCommand(command, callback))
						}
					/>
					<Dimmer active={this.denied} inverted/>
				</Segment>
			</Fragment>
		);
	}

	async onCommand(command, callbackPrint) {
		let directory = this.executor.instance.directory;
		command = ScriptParser.parseArguments(command.join(' ')).join(' ');

		let scriptAnswer = await this.api.executeCommand({ command, directory });

		if (!scriptAnswer) {
			callbackPrint(this.t('no-response') /*'No response from remote!'*/);
			return;
		}

		this.executor.instance.update({ directory: scriptAnswer.directory });
		callbackPrint(scriptAnswer.output);
	}
}

export default withRouter(
	connect(
		redux.with('auth'),
		redux.withToaster()
	)(withTranslation('WebTerminal')(WebTerminal))
);
