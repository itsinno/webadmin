/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _ from 'lodash';
import React from 'react';
import { parseSearch, redux } from '../utils/misc';
import { AC_LOGIN, Action } from '../redux/actions';
import { connect } from 'react-redux';
import ApiResolver from '../api/ApiResolver';
import User from '../api/auth/User';
import { withRouter } from 'react-router-dom';
import { Dimmer, Loader } from 'semantic-ui-react';

class Authorize extends React.Component {
	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get anonymous() {
		return this.user.isAnonymous();
	}

	get login() {
		return this.props.login || ($ => $);
	}

	get history() {
		return (
			this.props.history || {
				push: $ => $
			}
		);
	}

	get location() {
		return this.props.location || {};
	}

	get params() {
		return parseSearch(this.location.search);
	}

	get toaster() {
		return {
			openToast: (...args) => this.props.openToast(...args),
			closeToast: (...args) => this.props.closeToast(...args)
		};
	}

	async componentDidMount() {
		const { signature, expiresOn, from } = this.params;
		let user = User.fromObject({ signature: { signature, expiresOn } });

		console.log(user);

		if (this.anonymous) {
			user = await ApiResolver.with(user, this.toaster).users.getMe();
			if (!user) user = {};

			user = User.fromObject({ ...user, signature: { signature, expiresOn } });
			await this.login(user);
			await this.history.push(from);
		} else {
			await this.history.push(from);
		}
	}

	render() {
		return (
			<Dimmer active>
				<Loader size={'huge'}>Authorizing...</Loader>
			</Dimmer>
		);
	}
}

export default withRouter(
	connect(
		redux.with('auth'),
		{
			login: Action.for(AC_LOGIN).action,
			...redux.withToaster()
		}
	)(Authorize)
);
