/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, { Fragment } from 'react';
import {
	Button,
	ButtonGroup,
	Container,
	Dimmer,
	Divider,
	Form,
	FormInput,
	Grid,
	GridColumn,
	Header,
	Icon,
	Loader,
	Modal,
	ModalActions,
	ModalContent,
	ModalHeader
} from 'semantic-ui-react';
import { parseSearch, redux } from '../utils/misc';
import { AC_LOGIN, Action } from '../redux/actions';
import { connect } from 'react-redux';
import ApiResolver from '../api/ApiResolver';
import User from '../api/auth/User';
import { Redirect, withRouter } from 'react-router-dom';
import InnoButton from '../components/sign-in/InnoButton';
import { withTranslation } from 'react-i18next';
import _ from 'lodash';

let DEFAULT_STATE = {
	email: '',
	password: '',
	loading: false
};

class SignIn extends React.Component {
	constructor(props) {
		super(props);
		this.state = DEFAULT_STATE;
	}

	get t() {
		return this.props.t;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get anonymous() {
		return this.user.isAnonymous();
	}

	get history() {
		return this.props.history || { goBack: $ => $ };
	}

	get location() {
		return this.props.location || {};
	}

	get params() {
		return parseSearch(this.location.search);
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).auth;
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast
		};
	}

	render() {
		return (
			<Modal open size={'small'}>
				<ModalHeader>
					<Icon name={'sign-in'} /> {this.t('title')}
				</ModalHeader>
				{this.anonymous ? <Fragment /> : <Redirect to={this.params.from} />}
				<ModalContent>
					<Dimmer active={this.state.loading}>
						<Loader />
					</Dimmer>
					<Container fluid style={{ position: 'relative' }}>
						<Grid columns={2} relaxed={'very'}>
							<GridColumn>
								<Form onSubmit={async () => await this.performLogin()}>
									<FormInput
										disabled
										label={this.t('form.email')}
										value={this.state.email}
										onChange={async ({ target }) =>
											await this.changeForm('email', target.value)
										}
									/>
									<FormInput
										disabled
										type={'password'}
										label={this.t('form.password')}
										value={this.state.password}
										onChange={async ({ target }) =>
											await this.changeForm('password', target.value)
										}
									/>
								</Form>
							</GridColumn>
							<GridColumn>
								<Header size={'tiny'} content={this.t('sign-in-with.title')} />
								<InnoButton from={this.params.from} />
							</GridColumn>
						</Grid>
						<Divider vertical>{this.t('sign-in-with.or')}</Divider>
					</Container>
				</ModalContent>
				<ModalActions>
					<ButtonGroup>
						<Button
							color={'grey'}
							content={this.t('form.back')}
							icon={'arrow left'}
							labelPosition={'left'}
							onClick={async () => await this.goBack()}
						/>
						<Button
							disabled
							color={'green'}
							icon={'sign-in'}
							content={this.t('form.sign-in')}
							labelPosition={'right'}
							loading={this.state.loading}
							onClick={async () => await this.performLogin()}
						/>
					</ButtonGroup>
				</ModalActions>
			</Modal>
		);
	}

	async loading(loading) {
		await this.setState({ loading });
	}

	async performLogin() {
		let { email, password } = this.state;
		await this.loading(true);
		let user = await this.api.authorize({ email, password });
		await this.loading(false);
		await this.props.login(user);
	}

	async goBack() {
		await this.history.goBack();
	}

	async changeForm(type, value) {
		await this.setState({ [type]: value });
	}
}

export default withRouter(
	connect(
		redux.with('auth'),
		{
			login: Action.for(AC_LOGIN).action,
			...redux.withToaster()
		}
	)(withTranslation('SignIn')(SignIn))
);
