/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React from 'react';
import { Divider, Header, Segment } from 'semantic-ui-react';
import BotList from '../components/bots/BotList';
import { withTranslation } from 'react-i18next';

class Bots extends React.Component {
	get t() {
		return this.props.t;
	}

	render() {
		return (
			<Segment raised padded>
				<Header content={this.t('bots') /*Bots*/} />
				<Divider />
				<BotList />
			</Segment>
		);
	}
}

export default withTranslation('Bots')(Bots);
