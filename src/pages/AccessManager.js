/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React from 'react';
import UsersList from '../components/access-manager/UsersList';
import { Divider, Header, Segment } from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class AccessManager extends React.Component {
	get t() {
		return this.props.t;
	}

	render() {
		return (
			<Segment padded raised>
				<Header content={this.t('access-manager') /*'Access manager'*/} />
				<Divider />
				<UsersList />
			</Segment>
		);
	}
}

export default withTranslation('AccessManager')(AccessManager);
