/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React from 'react';
import { Container, Segment } from 'semantic-ui-react';
import ArticleList from '../components/knowledge-base/ArticleList';
import { withRouter } from 'react-router-dom';
import { buildLocation } from '../utils/misc';

class KnowledgeBase extends React.Component {
	get here() {
		return buildLocation(this.props.location);
	}

	render() {
		return (
			<Segment padded raised>
				<Container>
					<ArticleList query={this.props.query} />
				</Container>
			</Segment>
		);
	}
}

export default withRouter(KnowledgeBase);
