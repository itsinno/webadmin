/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React from 'react';
import { Header, Icon, Segment } from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class NotFound extends React.Component {
	get t() {
		return this.props.t;
	}

	render() {
		return (
			<Segment raised padded={'very'}>
				<Segment basic placeholder size={'huge'}>
					<Header icon>
						<Icon name={'find'} />
						{this.t(
							'does-not-exist'
						) /*Oops! Seems that such page does not exist.*/}
					</Header>
				</Segment>
			</Segment>
		);
	}
}

export default withTranslation('NotFound')(NotFound);
