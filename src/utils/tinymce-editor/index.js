import tinymce from 'tinymce/tinymce';

import './styles';
import 'tinymce/themes/silver';
import './plugins';

window.tinymce = tinymce;
