/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import 'tinymce/plugins/anchor';
import 'tinymce/plugins/image';
import 'tinymce/plugins/table';
import 'tinymce/plugins/insertdatetime';
import 'tinymce/plugins/hr';
import 'tinymce/plugins/media';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/paste';