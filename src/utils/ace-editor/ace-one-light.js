/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import ace from 'brace';

// noinspection JSCheckFunctionSignatures,JSUnusedLocalSymbols
ace.define('ace/theme/one-light', ['require', 'exports', 'module', 'ace/lib/dom'], function (acequire, exports, module) {

	exports.isDark = false;
	exports.cssClass = 'ace-one-light';
	exports.cssText = `.ace-one-light .ace_gutter {
  background: #fafafa;
  color: rgb(153,154,158)
}

.ace-one-light .ace_print-margin {
  width: 1px;
  background: #e8e8e8
}

.ace-one-light {
  background-color: #fafafa;
  color: #383a42
}

.ace-one-light .ace_cursor {
  color: #526fff
}

.ace-one-light .ace_marker-layer .ace_selection {
  background: #a9beef
}

.ace-one-light.ace_multiselect .ace_selection.ace_start {
  box-shadow: 0 0 3px 0px #fafafa;
  border-radius: 2px
}

.ace-one-light .ace_marker-layer .ace_step {
  background: rgb(198, 219, 174)
}

.ace-one-light .ace_marker-layer .ace_bracket {
  margin: -1px 0 0 -1px;
  border: 1px solid #d5d6d7
}

.ace-one-light .ace_marker-layer .ace_active-line {
  background: rgba(0, 0, 0, 0.024)
}

.ace-one-light .ace_gutter-active-line {
  background-color: rgba(0, 0, 0, 0.024)
}

.ace-one-light .ace_marker-layer .ace_selected-word {
  border: 1px solid #a9beef
}

.ace-one-light .ace_fold {
  background-color: #4078f2;
  border-color: #383a42
}

.ace-one-light .ace_keyword {
  color: #a626a4
}

.ace-one-light .ace_keyword.ace_operator {
  color: #383a42
}

.ace-one-light .ace_keyword.ace_other.ace_unit {
  color: #986801
}

.ace-one-light .ace_constant {
  color: #986801
}

.ace-one-light .ace_constant.ace_numeric {
  color: #986801
}

.ace-one-light .ace_constant.ace_character.ace_escape {
  color: #0184bc
}

.ace-one-light .ace_support.ace_function {
  color: #0184bc
}

.ace-one-light .ace_support.ace_class {
  color: #c18401
}

.ace-one-light .ace_support.ace_type {
  color: #0184bc
}

.ace-one-light .ace_storage {
  color: #a626a4
}

.ace-one-light .ace_invalid.ace_illegal {
  color: #ffffff;
  background-color: #ff1414
}

.ace-one-light .ace_invalid.ace_deprecated {
  color: #000000;
  background-color: #f2a60d
}

.ace-one-light .ace_string {
  color: #50a14f
}

.ace-one-light .ace_string.ace_regexp {
  color: #0184bc
}

.ace-one-light .ace_comment {
  font-style: italic;
  color: #a0a1a7
}

.ace-one-light .ace_variable {
  color: #e45649
}

.ace-one-light .ace_variable.ace_parameter {
  color: #383a42
}

.ace-one-light .ace_meta.ace_tag {
  color: #383a42
}

.ace-one-light .ace_meta.ace_selector {
  color: #a626a4
}

.ace-one-light .ace_entity.ace_other.ace_attribute-name {
  color: #986801
}

.ace-one-light .ace_entity.ace_name.ace_function {
  color: #4078f2
}

.ace-one-light .ace_entity.ace_name.ace_tag {
  color: #e45649
}

.ace-one-light .ace_markup.ace_heading {
  color: #e45649
}`;

	const dom = acequire('../lib/dom');
	// noinspection JSUnresolvedFunction
	dom.importCssString(exports.cssText, exports.cssClass);
});
