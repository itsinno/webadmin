/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

// This file is required since React build can't handle dynamic imports
//      and brace doesn't have any option to include all modes.

import 'brace/mode/ruby';
import 'brace/mode/sh';
import 'brace/mode/powershell';
import 'brace/mode/perl';
import 'brace/mode/php';
import 'brace/mode/python';
import 'brace/mode/javascript';