/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _            from 'lodash';
import ScriptParser from '../web-terminal/ScriptParser';
import pluralize    from 'pluralize';

class Option {
	constructor(prefix, value) {
		this.prefix = prefix;
		this.value = value;
	}

	get category() {
		return pluralize(this.prefix);
	}

	static probe(part) {
		let [prefix, value] = part.split(':', 2);
		if (!_.isString(value)) return part;
		return new Option(prefix, value);
	}
}

export default class SearchParser {
	static parse(searchQuery) {
		let query = {};
		let rest = [];

		let queryParts = ScriptParser.parseArguments(searchQuery)
			.map(part => Option.probe(part));

		for (let part of queryParts) {
			if (part instanceof Option) {
				(query[part.category] = query[part.category] || [])
					.push(part.value);
			} else {
				rest.push(part);
			}
		}

		return {...query, string: rest.join(' ')};
	}
}
