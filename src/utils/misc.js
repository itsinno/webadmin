/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _ from 'lodash';
import { fromString as uuidv5 } from 'uuidv4';
import { AC_TOAST_PULL, AC_TOAST_PUSH, Action } from '../redux/actions';

const LANG_MAP = {
	unknown: {
		flag: 'va',
		text: 'Unknown'
	},
	en: {
		flag: 'uk',
		text: 'English'
	},
	ru: {
		flag: 'ru',
		text: 'Russian'
	}
};

export class Timer {
	constructor(delay) {
		this.delay = delay;
	}

	// noinspection JSUnusedGlobalSymbols
	then(resolve) {
		setTimeout(resolve, this.delay);
	}
}

export const UID = string => uuidv5(string || '');

export const quote = value => '"' + _.toString(value) + '"';

export const dropdownOption = (option, text) => ({
	key: UID(option),
	value: option,
	text: text || option
});

export const languageOption = option => {
	const opt = LANG_MAP[option] || LANG_MAP.unknown;

	return {
		...opt,
		key: UID(option),
		value: option
	};
};

export const redux = {
	with: prop => state => ({ [prop]: state[prop] }),
	withToaster: () => ({
		openToast: Action.for(AC_TOAST_PUSH).action,
		closeToast: Action.for(AC_TOAST_PULL).action
	})
};

export const buildLocation = location =>
	`${location.pathname}${location.search}${location.hash}`;

export const parseSearch = searchString => {
	const preparsed = new URLSearchParams(searchString);
	const parsed = {};

	for (const [key, value] of preparsed) {
		parsed[_.camelCase(key)] = value;
	}

	return parsed;
};

export const nullify = () => null;

export const fitValue = ({ min, max }) => value =>
	Math.max(min, Math.min(max, value));
