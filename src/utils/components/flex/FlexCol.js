/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React from 'react';
import _     from 'lodash';

const DEFAULT_PROPS = {
	inline: false,
	direction: 'row',
	wrap: true,
	flex: false,
	align: 'flex-start'
};

export default class FlexCol extends React.Component {

	get style() {
		let _style = _.cloneDeep(this.props.style) || {};
		return {...this.flexStyle, ..._style};
	}

	get flexStyle() {
		return {
			flex: this.defaultProp('flex'),
			flexGrow: this.defaultProp('grow'),
			flexShrink: this.defaultProp('shrink'),
			flexBasis: this.defaultProp('basis'),
			alignSelf: this.defaultProp('align')
		};
	}

	defaultProp(prop) {
		return _.isNil(this.props[prop]) ? DEFAULT_PROPS[prop] : this.props[prop];
	}

	render() {
		return <div style={this.style} className={'flex-react flex-col'}>
			{this.props.children}
		</div>;
	}
}
