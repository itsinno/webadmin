/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _     from 'lodash';
import React from 'react';

const DEFAULT_PROPS = {
	inline: false,
	direction: 'row',
	wrap: true,
	flex: false
};

export default class FlexRow extends React.Component {
	get wrapping() {
		let wrap = this.defaultProp('wrap');
		if (wrap === 'reverse') return 'wrap-reverse';
		return wrap ? 'wrap' : 'nowrap';
	}

	get style() {
		let _style = _.cloneDeep(this.props.style) || {};
		return {...this.flexStyle, ..._style};
	}

	get flexStyle() {
		return {
			display: this.defaultProp('inline') ? 'inline-flex' : 'flex',
			direction: this.defaultProp('direction'),
			wrap: this.wrapping,
			flex: this.defaultProp('flex'),
			alignItems: this.defaultProp('alignItems'),
			justifyContent: this.defaultProp('justifyContent'),
			alignContent: this.defaultProp('alignContent')
		}
	}

	defaultProp(prop) {
		return _.isNil(this.props[prop]) ? DEFAULT_PROPS[prop] : this.props[prop];
	}

	render() {
		return <div className={'flex-react flex-row'} style={this.style}>
			{this.props.children}
		</div>;
	}
}
