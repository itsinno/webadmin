export const MT_FAILURE = 16;
export const MT_WARNING = 8;
export const MT_SUCCESS = 4;
export const MT_INFO = 2;
export const MT_OMIT = 1;
