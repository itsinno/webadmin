/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React     from 'react';
import {Segment} from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class CloseAll extends React.Component {

	get t() {
		return this.props.t;
	}

	render() {
		return (
			<Segment raised textAlign={'center'}>
				<a href={'#'} // eslint-disable-line jsx-a11y/anchor-is-valid
				   onClick={async () => await this.onCloseAll()()}
				>
					{this.t('close-all')/*CLOSE ALL*/}
				</a>
			</Segment>
		);
	}

	onCloseAll = () => this.props.onCloseAll || ($ => $);
}

export default withTranslation('externals/toaster/toaster/CloseAll')(CloseAll);