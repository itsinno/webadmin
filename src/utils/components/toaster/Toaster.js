/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                 from 'lodash';
import React, {Fragment} from 'react';
import Toast             from './Toast';
import {TransitionGroup} from 'semantic-ui-react';
import CloseAll          from './toaster/CloseAll';
import {UID}             from '../../misc';

const DEFAULT_CAPACITY = 3;

export default class Toaster extends React.Component {
	get toasts() {
		return _.takeRight(this.props.toasts, this.capacity);
	}

	get capacity() {
		return this.props.capacity || DEFAULT_CAPACITY;
	}

	renderToast(toast) {
		return <Toast
			{...toast}
			key={toast.id}
			onClose={async () => await this.closeToast(toast.id)}
		/>;
	}

	render() {
		return <div className={'ui toaster'}>
			<TransitionGroup animation={'fly left'}>
				{this.toasts.map((toast) => this.renderToast(toast))}
				{this.toasts.length > 0 ?
					<CloseAll key={UID('toastCloser')} onCloseAll={async () => await this.closeAll()}/> : <Fragment/>}
			</TransitionGroup>
		</div>;
	}

	onToastClosed = () => this.props.onToastClosed || (async value => value);

	async closeToast(key) {
		await this.onToastClosed()(key);
	}

	async closeAll() {
		const promises = this.props.toasts.map((toast) => this.closeToast(toast.id));

		await Promise.all(promises);
	}
}
