/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                                                        from 'lodash';
import React                                                    from 'react';
import { Button, Header, Segment }                              from 'semantic-ui-react';
import uuid                                                     from 'uuidv4';
import { Timer }                                                from '../../misc';
import { MT_FAILURE, MT_INFO, MT_OMIT, MT_SUCCESS, MT_WARNING } from './types';

const displayProps = (type) => {
	switch (type) {
	case MT_FAILURE:
		return { color: 'red' };
	case MT_INFO:
		return { color: 'blue' };
	case MT_SUCCESS:
		return { color: 'green' };
	case MT_WARNING:
		return { color: 'yellow' };
	case MT_OMIT:
	default:
		return { color: 'grey' };
	}
};

const COMMON_PROPS = {
	inverted: false,
	raised: true
};
const CLASS_NAME = 'ui toast';

export default class Toast extends React.Component {

	static withId(toast = {}) {
		return { id: uuid(), ...toast };
	}

	get passedProps() {
		return _.merge(
			COMMON_PROPS,
			displayProps(this.props.type),
			this.classNameObject
		);
	}

	get classNameObject() {
		return { className: [CLASS_NAME, this.props.className].join(' ') };
	}

	componentDidMount() {
		if (this.props.lifetime) {
			// noinspection JSIgnoredPromiseFromCall
			this.killAfter(this.props.lifetime);
		}
	}

	async killAfter(delay) {
		await new Timer(delay);
		await this.onClose()();
	}

	render() {
		return <Segment {...this.passedProps}>
			<Header content={this.props.header} size={'small'}/>
			<span>{this.props.message}</span>
			<Button
				basic
				circular
				size={'mini'}
				icon={'close'}
				className={'close'}
				inverted={COMMON_PROPS.inverted}
				onClick={this.onClose()}
			/>
		</Segment>;
	}

	onClose = () => this.props.onClose || (async value => value);

}
