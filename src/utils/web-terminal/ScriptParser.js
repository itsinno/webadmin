/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _ from 'lodash';
import { quote } from '../misc';

export default class ScriptParser {
	constructor(script) {
		this.script = script;
	}

	static parseArguments(args) {
		const state = {
			quoteOpen: false,
			args: [],
			current: ''
		};
		for (let i = 0; i < args.length; i++) {
			if (/["']/.test(args[i])) {
				if (state.quoteOpen) {
					state.args.push(state.current);
					state.current = '';
				}
				state.quoteOpen = !state.quoteOpen;
			} else if (/\s/.test(args[i])) {
				if (state.quoteOpen) {
					state.current += args[i];
					continue;
				}
				if (state.current.length > 0) {
					state.args.push(state.current);
					state.current = '';
				}
			} else {
				state.current += args[i];
			}
		}
		if (state.current.length > 0) {
			state.args.push(state.current);
			state.current = '';
		}

		return state.args;
	}

	/* TODO */
	// verify(values) {
	// 	let result = (succ, reas) => ({
	// 		success: succ,
	// 		reason: reas,
	// 	});
	// }

	compile(values = {}) {
		values = _.cloneDeep(values);

		values.args = values.args || '';
		values.flags = values.flags || {};

		let command = `"${this.script.command}"`;

		let flags = [];
		for (let flag of this.script.flags || []) {
			flags.push(this.processFlag(values, flag));
		}
		flags = _.compact(flags).join(' ');

		let args = ScriptParser.parseArguments(values.args);
		args = _.map(args, arg => `"${arg}"`);

		return _.concat([command], flags, args).join(' ');
	}

	processFlag(values, flag) {
		let value = values.flags[flag.name];
		if (_.isNil(value)) return null;

		if (flag.type === 'boolean') return value ? flag.name : '';

		if (!_.some(_.keys(values.flags), value => value === flag.name))
			return null;
		return [flag.name, quote(value)].join(this.script.flagSeparator);
	}
}
