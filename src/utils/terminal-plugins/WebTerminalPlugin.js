/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _          from 'lodash';
import PluginBase from 'terminal-in-react/lib/js/components/Plugin';
import Config     from '../../config/base';

const PROMPT_SYM = Config.webTerminal.terminal.plugin.promptSymbol;

const WebTerminalPlugin = (plugin) => class $WebTerminalPlugin extends PluginBase {
	static displayName = 'external-executor';
	// noinspection JSUnusedGlobalSymbols
	static version = '1.0.0';

	constructor(api, config = {}) {
		super(api, config);

		this.api = api;
		this.config = config;

		this.state = {
			user: '',
			host: '',
			directory: ''
		};

		// noinspection JSUnusedGlobalSymbols
		this.getPublicMethods = () => ({});
		// noinspection JSUnusedGlobalSymbols
		this.readStdOut = () => true;

		// exporting plugin instance outside
		plugin.instance = this;
	}

	get directory() {
		return this.state.directory || '~';
	}

	// noinspection JSUnusedGlobalSymbols
	updateApi(newApi) {
		this.api = newApi;
	}

	printMessage(message) {
		this.api.printLine(message);
	}

	executeCommand(command) {
		this.api.printLine(this.getPromptPrefix() + PROMPT_SYM + ' ' + command);
		this.api.runCommand(command);
	}

	update({user, host, directory} = {}) {
		this.setState({user, host, directory});
	}

	setState(state) {
		this.state = _.merge(this.state, state);
		this.updatePrompt();
	}

	updatePrompt() {
		const promptPrefix = this.getPromptPrefix();
		this.api.setPromptPrefix(promptPrefix);
		this.api.setPromptSymbol(PROMPT_SYM);
	}

	getPromptPrefix() {
		return `${this.state.host || 'server'}@${this.state.user || 'user'} ${this.directory} `;
	}
};

// noinspection JSUnusedGlobalSymbols
export default WebTerminalPlugin;

export const defaultWTPlugin = () => {
	let wtPlugin = {};
	wtPlugin.class = WebTerminalPlugin(wtPlugin);
	return wtPlugin;
};

