/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import {AC_TOAST_PULL, AC_TOAST_PUSH} from '../actions';
import _                              from 'lodash';
import uuid                           from 'uuidv4';
import DEFAULT_STATE                  from '../default';

const toast = {
	push: (toasts, toast = {}) =>
		_.concat(toasts || [], {id: uuid(), ...toast}),
	pull: (toasts, {id} = {}) =>
		_.filter(toasts || [], toast => toast.id !== id)

};

const toastReducer = (state = DEFAULT_STATE.toasts, {type, data}) => {
	switch (type) {
	case AC_TOAST_PUSH:
		return toast.push(state, data);
	case AC_TOAST_PULL:
		return toast.pull(state, data);
	default:
		return state;
	}
};


export default toastReducer;
