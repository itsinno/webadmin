/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import authReducer       from './auth';
import toastReducer      from './toast';
import {combineReducers} from 'redux';

const reducers = {
	auth: authReducer,
	toasts: toastReducer
};


const rootReducer = combineReducers(reducers);
export default rootReducer;
