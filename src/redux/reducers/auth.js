/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import {AC_LOGIN, AC_LOGOUT} from '../actions';
import User                  from '../../api/auth/User';
import DEFAULT_STATE         from '../default';

const login = (state = {}, user) => ({...state, user});

const logout = (state = {}) => ({...state, user: User.anonymous});

const authReducer = (state = DEFAULT_STATE.auth, {type, data}) => {
	switch (type) {
	case AC_LOGIN:
		return login(state, data);
	case AC_LOGOUT:
		return logout(state, data);
	default:
		return state;
	}
};

export default authReducer;
