export const AC_LOGIN = 'AC_LOGIN';
export const AC_LOGOUT = 'AC_LOGOUT';
export const AC_TOAST_PUSH = 'AC_TOAST_PUSH';
export const AC_TOAST_PULL = 'AC_TOAST_PULL';

export class Action {
	constructor(type) {
		this._type = type;
	}

	get action() {
		return data => this.with(data);
	}

	/**
	 * @return {Action}
	 */
	static for(type) {
		this._actions = this._actions || {};
		return this._actions[type] || (this._actions[type] = new Action(type));
	}

	with(data) {
		return {
			type: this._type,
			data
		};
	}
}
