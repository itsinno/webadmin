/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import { createStore } from 'redux';
import DEV_TOOLS from './devtools';
import { persistReducer, persistStore } from 'redux-persist';
import rootReducer from './reducers';
import storage from 'redux-persist/lib/storage';
import DEFAULT_STATE from './default';
import transforms from './persistence/transforms';

const persistConfig = {
	storage,
	key: 'root',
	transforms
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export let store = createStore(persistedReducer, DEFAULT_STATE, DEV_TOOLS);

export let persistor = persistStore(store);
