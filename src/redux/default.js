/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import User from '../api/auth/User';

export let DEFAULT_STATE = {
	auth: {
		user: User.anonymous
	},
	toasts: []
};

export default DEFAULT_STATE;
