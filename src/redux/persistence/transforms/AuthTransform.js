/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import {createTransform} from 'redux-persist';
import User              from '../../../api/auth/User';

const AuthTransform = createTransform(
	(state, key) => {
		switch (key) {
		case 'auth':
			return {...state, user: state.user.object};
		case 'toasts':
		default:
			return state;
		}
	},
	(state, key) => {
		switch (key) {
		case 'auth':
			return {...state, user: User.fromObject(state.user)};
		case 'toasts':
		default:
			return state;
		}
	}
);

export default AuthTransform;
