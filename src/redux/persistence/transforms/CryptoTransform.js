/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import {createTransform} from 'redux-persist';
import createEncryptor   from 'redux-persist-transform-encrypt';
import Config            from '../../../config/base';
import _                 from 'lodash';

const CryptoTransform = _.isString(Config.persistence.secretKey) ? createEncryptor({
	secretKey: Config.persistence.secretKey,
	onError: () => {

	}
}) : createTransform(state => state, state => state);

export default CryptoTransform;
