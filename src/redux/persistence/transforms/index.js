/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import AuthTransform   from './AuthTransform';
import CryptoTransform from './CryptoTransform';

export default [AuthTransform, CryptoTransform];
