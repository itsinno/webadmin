/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import BasicApi             from './BasicApi';
import ApiProvider          from '../provider/ApiProvider';
import {BOTS_SERVER_CONFIG} from '../../config/api';

export default class StatsApi extends BasicApi {
	constructor(credentials, toaster) {
		super(credentials, toaster);
		this.botsProvider = new ApiProvider(credentials, BOTS_SERVER_CONFIG);
	}

	async botsResolve({endpoint, params}) {
		return this.postProcess(await this.botsProvider.resolve({endpoint, params}));
	}

	// noinspection JSMethodCanBeStatic
	async botsStatus() {
		return this.botsResolve({
			endpoint: 'bots.countAlive'
		});
	}

	// noinspection JSMethodCanBeStatic
	async ticketsStatus() {
		return this.fetch({
			endpoint: 'stats.ticketsStatus'
		});
	}

	// noinspection JSMethodCanBeStatic
	async ticketStats({period}) {
		return this.fetch({
			endpoint: 'stats.ticketsStats',
			params: {period}
		});
	}

	// noinspection JSMethodCanBeStatic
	async engineersTop({period, count}) {
		return this.fetch({
			endpoint: 'stats.topEngineers',
			params: {period, count}
		});
	}

	// noinspection JSMethodCanBeStatic
	async ticketCategoriesTop({period, count}) {
		return this.fetch({
			endpoint: 'stats.topCategories',
			params: {period, count}
		});
	}
}
