/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import BasicApi from './BasicApi';

export default class KnowledgeBaseApi extends BasicApi {

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async getArticle({id} = {}) {
		return this.fetch({
			endpoint: 'knowledgeBase.getArticle',
			params: {id}
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async getArticles({page, pageSize} = {}) {
		return this.fetch({
			endpoint: 'knowledgeBase.getArticles',
			params: {page, pageSize}
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async countFoundArticles({query} = {}) {
		return this.fetch({
			endpoint: 'knowledgeBase.countFoundArticles',
			params: query
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async searchArticles({query, page, pageSize} = {}) {
		return this.fetch({
			endpoint: 'knowledgeBase.searchArticles',
			params: {...query, page, pageSize}
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async countArticles() {
		return this.fetch({
			endpoint: 'knowledgeBase.countArticles'
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async deleteArticle({id} = {}) {
		return this.fetch({
			endpoint: 'knowledgeBase.deleteArticle',
			params: {id}
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async changeArticle({article} = {}) {
		return this.fetch({
			endpoint: 'knowledgeBase.changeArticle',
			params: article
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async addArticle({article} = {}) {
		return this.fetch({
			endpoint: 'knowledgeBase.addArticle',
			params: article
		});
	}
}
