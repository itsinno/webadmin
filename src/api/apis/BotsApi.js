/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import BasicApi             from './BasicApi';
import ApiProvider          from '../provider/ApiProvider';
import {BOTS_SERVER_CONFIG} from '../../config/api';

class BotsApi extends BasicApi {
	constructor(cred, toaster) {
		super(cred, toaster);
		// noinspection JSUnusedGlobalSymbols
		this.provider = new ApiProvider(cred, BOTS_SERVER_CONFIG);
	}


	async getBots() {
		return this.fetch({
			endpoint: 'bots.getBots',
			params: {messenger: null}
		});
	}

	async stopBot({messenger} = {}) {
		return this.fetch({
			endpoint: 'bots.stop',
			params: {messenger}
		});
	}

	async startBot({messenger} = {}) {
		return this.fetch({
			endpoint: 'bots.start',
			params: {messenger}
		});
	}

	async getBotUsers({messenger, page, pageSize} = {}) {
		return this.fetch({
			endpoint: 'bots.getUsers',
			params: {messenger, pageSize, page}
		});
	}

	async countBotUsers({messenger} = {}) {
		return this.fetch({
			endpoint: 'bots.countUsers',
			params: {messenger}
		});
	}

	async dropBotUser({email} = {}) {
		return this.fetch({
			endpoint: 'bots.logoutUsers',
			params: [email]
		});
	}

	async dropBotUsers(emails = {}) {
		return this.fetch({
			endpoint: 'bots.logoutUsers',
			params: emails
		});
	}
}

export default BotsApi;
