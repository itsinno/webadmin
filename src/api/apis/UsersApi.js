/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import BasicApi from './BasicApi';

export default class UsersApi extends BasicApi {
	async getUsers({search, page, pageSize} = {}) {
		return this.fetch({
			endpoint: 'users.getUsers',
			params: {search, page, pageSize}
		});
	}

	async countUsers({search} = {}) {
		return this.fetch({
			endpoint: 'users.countUsers',
			params: {search}
		});
	}

	async getMe() {
		return this.fetch({
			endpoint: 'users.getUser',
			params: {email: null}
		});
	}

	async getUser({email} = {}) {
		return this.fetch({
			endpoint: 'users.getUser',
			params: {email}
		});
	}

	async setAuthority({email, authority} = {}) {
		return this.fetch({
			endpoint: 'users.setAuthority',
			params: {email, authority}
		});
	}

}
