/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import BasicApi from './BasicApi';

export default class WebTerminalApi extends BasicApi {
	// noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols,JSMethodCanBeStatic
	async getScriptTemplate({id} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.getScriptTemplate',
			params: {id}
		});
	}

	// noinspection JSUnusedLocalSymbols,JSMethodCanBeStatic
	async getScriptTemplates({page, pageSize} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.getScriptTemplates',
			params: {page, pageSize}
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async countScriptTemplates() {
		return this.fetch({
			endpoint: 'webTerminal.countScriptTemplates'
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async deleteScriptTemplate({id} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.deleteScriptTemplate',
			params: {id}
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async changeScriptTemplate({script} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.changeScriptTemplate',
			params: script
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async addScriptTemplate({script} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.addScriptTemplate',
			params: script
		});
	}

	// noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols,JSMethodCanBeStatic
	async getScript({id} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.getScript',
			params: {id}
		});
	}

	// noinspection JSUnusedLocalSymbols,JSMethodCanBeStatic
	async getScripts({page, pageSize} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.getScripts',
			params: {page, pageSize}
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async countScripts() {
		return this.fetch({
			endpoint: 'webTerminal.countScripts'
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async deleteScript({id} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.deleteScript',
			params: {id}
		});
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async changeScript({script} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.changeScript',
			params: script
		});

	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async addScript({script} = {}) {
		return this.fetch({
			endpoint: 'webTerminal.addScript',
			params: script
		});

	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async executeCommand({command, directory}) {
		return this.fetch({
			endpoint: 'webTerminal.executeCommand',
			params: {command, directory}
		});

	}
}
