/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _ from 'lodash';
import ApiProvider, { AP_ERROR } from '../provider/ApiProvider';
import MAIN_SERVER_CONFIG from '../../config/api';
import { MT_FAILURE, MT_WARNING } from '../../utils/components/toaster/types';

const ERR_LIFETIME = 10000;

export default class BasicApi {
	constructor(credentials, toaster) {
		this.provider = new ApiProvider(credentials, MAIN_SERVER_CONFIG);
		this.toaster = toaster;
	}

	async fetch({ endpoint, params }) {
		return this.postProcess(await this.provider.resolve({ endpoint, params }));
	}

	postProcess({ status, data }) {
		if (!status || status === AP_ERROR) {
			this.toaster.openToast({
				type: MT_FAILURE,
				header: 'Internal error occured!',
				message: data.message,
				lifetime: ERR_LIFETIME
			});

			return null;
		} else if (!data.success) {
			this.toaster.openToast({
				type: MT_WARNING,
				header: 'Internal error occured!',
				message: _.get(data, 'error.message', ''),
				lifetime: ERR_LIFETIME
			});
		}

		return data.data;
	}
}
