/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import User                 from '../auth/User';
import Config               from '../../config/base';
import {AP_ERROR}           from '../provider/ApiProvider';
import AnonymousApiProvider from '../provider/AnonymousApiProvider';
import {AUTH_SERVER_CONFIG} from '../../config/api';

const callbackUrl = (type, from) => new URL(`authorize?type=${type}&from=${from}`, Config.general.root).toString();
const postProcess = ({status, data}) => {
	if (!status || status === AP_ERROR) {
		return null;
	} else if (!data.success) {
		return null;
	}

	return data.data;
};


export default class AuthApi {
	constructor() {
		this.provider = new AnonymousApiProvider(AUTH_SERVER_CONFIG);
	}

	// noinspection JSMethodCanBeStatic,JSUnusedLocalSymbols
	async authorize({email, password}) {
		return User.anonymous;
	}

	async getAuthPageURL({type, from}) {
		return this.resolve({
			endpoint: 'auth.getAuthPageURL',
			params: {
				type,
				callback: callbackUrl(type, from),
				state: 'webadmin'
			}
		});
	}

	async resolve({endpoint, params}) {
		return postProcess(await this.provider.resolve({endpoint, params}));
	}
}
