/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

export default class TokenParser {
	static get parsers() {
		return TokenParser.types || {};
	}

	static register(parserClass) {
		this.parsers[parserClass.type] = parserClass;
	}

	static parse(token, type) {
		const parser = TokenParser.parsers(type);
		if (!parser) return null;

		return new parser(token).parse();
	}
}
