/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

export const AU_ANONYMOUS = {level: 0, name: 'Anonymous', color: 'grey', icon: 'user secret'};
export const AU_USER = {level: 1, name: 'User', color: 'blue', icon: 'blind'};
export const AU_ENGINEER = {level: 2, name: 'Engineer', color: 'violet', icon: 'user'};
export const AU_ADMIN = {level: 4, name: 'Administrator', color: 'orange', icon: 'doctor'};

const AUTHORITIES = [AU_ANONYMOUS, AU_USER, AU_ENGINEER, AU_ADMIN];

class Authority {
	constructor({level, name, color, icon}) {
		this._level = level;
		this._name = name;
		this._color = color;
		this._icon = icon;
	}

	static get user() {
		return Authority.create(AU_USER);
	}

	static get anonymous() {
		return Authority.create(AU_ANONYMOUS);
	}

	static get engineer() {
		return Authority.create(AU_ENGINEER);
	}

	static get admin() {
		return Authority.create(AU_ADMIN);
	}

	/**
	 * @return {Authority[]}
	 */
	static get all() {
		return AUTHORITIES.map((option) => Authority.create(option));
	}

	get icon() {
		return this._icon;
	}

	get color() {
		return this._color;
	}

	get name() {
		return this._name;
	}

	get level() {
		return this._level;
	}

	/**
	 *
	 * @param {User} user
	 * @return {Authority}
	 */
	static of(user) {
		return user.authority;
	}

	/**
	 * @param option Authority object
	 * @return {Authority}
	 */
	static create(option) {
		this.authorities = this.authorities || [];
		return this.authorities[option.level] || (this.authorities[option.level] = new Authority(option));
	}

	static byLevel(level) {
		return this.create({level});
	}

	/**
	 * @param {Authority} authority
	 * @returns {boolean}
	 */
	granted(authority) {
		return this.level >= authority.level;
	}

	/**
	 * @param {Authority} authority
	 * @returns {boolean}
	 */
	denied(authority) {
		return this.level < authority.level;
	}

	exactly(authority) {
		return this.level === authority.level;
	}
}

// Create all existing authorities to be used correctly by Authority.byLevel
/**
 * @type {Authority[]}
 */
export const Authorities = Authority.all;

export default Authority;