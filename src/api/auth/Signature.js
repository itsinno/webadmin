/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

export default class Signature {
	constructor({signature, expiresOn}) {
		this._signature = signature;
		this._expiresOn = expiresOn * 1000;
	}

	get valid() {
		return this._expiresOn <= Date.now();
	}

	get signature() {
		return this._signature;
	}

	get object() {
		return {
			expiresOn: this._expiresOn,
			signature: this._signature
		};
	}

	toString = () => this.signature;
}