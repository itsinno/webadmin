/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _         from 'lodash';
import Authority from './Authority';
import Signature from './Signature';

export default class User {
	constructor({id, email, name, authority, signature, avatar} = {}) {
		this._id = id;
		this._name = name;
		this._email = email;
		this._authority = authority;
		this._signature = signature;
		this._avatar = avatar;
	}

	static get anonymous() {
		return this._anonymous || (this._anonymous = new User({authority: Authority.anonymous}));
	}

	/**
	 * @return {User}
	 */
	static get engineer() {
		return this._engineer || (this._engineer = new User({authority: Authority.engineer}));
	}

	static get admin() {
		return this._admin || (this._admin = new User({authority: Authority.admin}));
	}

	/**
	 * @return {Authority}
	 */
	get authority() {
		return this._authority;
	}

	get name() {
		return this._name;
	}

	get avatar() {
		return this._avatar;
	}

	get email() {
		return this._email;
	}

	get signature() {
		return this._signature;
	}

	get id() {
		return this._id;
	}

	get object() {
		return {
			email: this.email,
			authority: this.authority.level,
			name: this.name,
			signature: _.get(this, 'signature.object', {})
		};
	}

	static fromObject(object) {
		return new User({
			...object,
			signature: new Signature(object.signature),
			authority: Authority.byLevel(object.authority)
		});
	}

	isAnonymous() {
		return this.authority.exactly(Authority.anonymous);
	}
}
