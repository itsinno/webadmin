/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import Signature from '../auth/Signature';

export default class ParserBase {
	constructor(token) {
		this.token = new Signature(this.constructor.type, token);
	}

	static get type() {
		return undefined;
	}

	parse() {
		throw new Error('Not implemented');
	}
}