/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import User       from '../auth/User';
import ParserBase from './ParserBase';

export default class InnoTokenParser extends ParserBase {
	static get type() {
		return 'innopolis.sso';
	}

	parse() {
		return new User({
			token: this.token
		});
	}
}