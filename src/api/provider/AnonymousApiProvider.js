/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _              from 'lodash';
import axios          from 'axios';
import DEFAULT_CONFIG from '../../config/api';
import {fitValue}     from '../../utils/misc';

export const AP_ERROR = -1;
const LOG_LEVELS = ['log', 'info', 'warn', 'error'];
const LL_BOUNDS = {min: 0, max: 3};

export default class AnonymousApiProvider {
	constructor(config) {
		this.config = config;
	}

	static get instance() {
		return new AnonymousApiProvider(DEFAULT_CONFIG);
	}

	async resolve({endpoint, params = {}}) {
		try {
			let request = {
				method: this.config.method,
				url: this.getEndpoint(endpoint),
				data: {
					data: params
				}
			};

			this.log(0, 'AnonymousApiProvider#resolve(%O): request object %O.', {endpoint, params}, request);
			this.log(1, 'AnonymousApiProvider#resolve(%s): request sent.', endpoint);

			const response = await axios(request);

			this.log(1, 'AnonymousApiProvider#resolve(%s): request succeeded.', endpoint);
			this.log(0, 'AnonymousApiProvider#resolve(%O): response object %O.', {endpoint, params}, response);

			return response;
		} catch (error) {
			this.log(3, 'AnonymousApiProvider#resolve(%s): request failed.', endpoint);
			this.log(0, 'AnonymousApiProvider#resolve(%O): %O.', {endpoint, params}, error);

			return {
				status: AP_ERROR,
				data: error
			};
		}
	}

	getEndpoint(endpointName) {
		let path = _.get(this.config.endpoints, endpointName);
		return new URL(path, this.config.root).toString();
	}

	log(level, ...data) {
		const logLevel = fitValue(LL_BOUNDS)(this.config.logLevel);
		const currentLevel = fitValue(LL_BOUNDS)(level);

		if (logLevel > currentLevel) return null;

		console[LOG_LEVELS[currentLevel]](...data);
	}
}