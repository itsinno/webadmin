/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import WebTerminalApi from './apis/WebTerminalApi';
import KnowledgeBaseApi from './apis/KnowledgeBaseApi';
import User from './auth/User';
import AuthApi from './apis/AuthApi';
import UsersApi from './apis/UsersApi';
import StatsApi from './apis/StatsApi';
import BotsApi from './apis/BotsApi';

export default class ApiResolver {
	constructor(user, toaster) {
		this.user = user;
		this.toaster = toaster;
	}

	/**
	 * @return {WebTerminalApi}
	 */
	get webTerminal() {
		return (
			this._webTerminal ||
			(this._webTerminal = new WebTerminalApi(
				this.user.signature,
				this.toaster
			))
		);
	}

	/**
	 * @return {KnowledgeBaseApi}
	 */
	get knowledgeBase() {
		return (
			this._knowledgeBase ||
			(this._knowledgeBase = new KnowledgeBaseApi(
				this.user.signature,
				this.toaster
			))
		);
	}

	get bots() {
		return (
			this._bots ||
			(this._bots = new BotsApi(this.user.signature, this.toaster))
		);
	}

	get stats() {
		return (
			this._stats ||
			(this._stats = new StatsApi(this.user.signature, this.toaster))
		);
	}

	/**
	 * @return {AuthApi}
	 */
	get auth() {
		return this._auth || (this._auth = new AuthApi());
	}

	/**
	 * @return {UsersApi}
	 */
	get users() {
		return (
			this._users ||
			(this._users = new UsersApi(this.user.signature, this.toaster))
		);
	}

	// noinspection JSMethodCanBeStatic,JSUnusedGlobalSymbols
	static with(user, toaster) {
		return new ApiResolver(user, toaster);
	}
}

export const _ApiResolver = ApiResolver.with(User.anonymous, {});
