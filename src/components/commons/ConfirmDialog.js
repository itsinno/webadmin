/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import { Button, Header, Icon, Modal, ModalActions, ModalContent } from 'semantic-ui-react';
import React                                                       from 'react';
import { withTranslation }                                         from 'react-i18next';

export class $ConfirmDialog extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			open: false,
			loading: false
		};
	}

	get t() {
		return this.props.t;
	}

	get strings() {
		return {
			modalHeader: '',
			text: '',
			cancel: this.t('cancel')/*'Cancel'*/,
			confirm: this.t('proceed')/*'Proceed'*/
		};
	}

	get icons() {
		return {
			modalHeader: 'question circle',
			cancel: 'cancel',
			confirm: 'checkmark'
		};
	}

	get trigger() {
		return React.cloneElement(this.props.trigger/*, {disabled: this.props.disabled}*/);
	}

	async afterConfirm() {
	}

	onConfirmed = () => this.props.onConfirmed || (async value => value);

	render() {
		return <Modal basic
		              trigger={this.trigger}
		              open={this.state.open}
		              onOpen={async () => await this.modalOpen(true)}
		              onClose={async () => await this.modalOpen(false)}>
			<Header icon={this.icons.modalHeader} content={this.strings.modalHeader}/>
			<ModalContent>
				<p>{this.strings.text}</p>
			</ModalContent>
			<ModalActions>
				<Button basic inverted color={'red'}
				        disabled={this.state.loading}
				        onClick={async () => await this.modalOpen(false)}>
					<Icon name={this.icons.cancel}/> {this.strings.cancel}
				</Button>
				<Button inverted color={'green'}
				        loading={this.state.loading}
				        onClick={async () => await this.confirm()}>
					<Icon name={this.icons.confirm}/> {this.strings.confirm}
				</Button>
			</ModalActions>
		</Modal>;
	}

	async modalOpen(open) {
		await this.setState({ open });
	}

	async loading(loading) {
		this.setState({ loading });
	}

	async confirm() {
		await this.loading(true);
		await this.afterConfirm();
		await this.modalOpen(false);
		await this.loading(false);
		await this.onConfirmed()();
	}
}

export default withTranslation('commons/ConfirmDialog')($ConfirmDialog);