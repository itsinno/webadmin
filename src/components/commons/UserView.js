/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React      from 'react';
import UserAvatar from './UserAvatar';
import User       from '../../api/auth/User';

class UserView extends React.Component {
	get user() {
		return this.props.user || User.anonymous;
	}

	render() {
		return <React.Fragment>
			<UserAvatar user={this.user}/>&nbsp;{this.user.name}
		</React.Fragment>;
	}
}

export default UserView;
