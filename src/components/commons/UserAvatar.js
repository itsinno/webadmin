/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import gravatar from 'gravatar';
import React    from 'react';
import {Image}  from 'semantic-ui-react';
import User     from '../../api/auth/User';

const GRAVATAR_CONFIG = {protocol: 'https', default: 'identicon'};

export default class UserAvatar extends React.Component {
	get avatar() {
		const gra = gravatar.url(this.user.email, GRAVATAR_CONFIG);

		return this.user.avatar || gra;
	}

	/**
	 * @return {User}
	 */
	get user() {
		return this.props.user || User.anonymous;
	}

	render() {
		return <Image avatar src={this.avatar}/>;
	}
}
