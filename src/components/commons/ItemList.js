/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
import React, {Fragment} from 'react';
import {
	Button,
	Divider,
	Header,
	Input,
	Loader,
	Pagination,
	Popup,
	Table,
	TableBody,
	TableCell,
	TableFooter,
	TableHeader,
	TableHeaderCell,
	TableRow
}                        from 'semantic-ui-react';
import _                 from 'lodash';
import FlexRow           from '../../utils/components/flex/FlexRow';
import FlexCol           from '../../utils/components/flex/FlexCol';
import whatkey           from 'whatkey';
import Config            from '../../config/base';


const DEFAULT_PAGE_SIZE = Config.itemList.defaultPageSize;
const PAGE_SIZE_RANGE = Config.itemList.pageSizeRange;

export class ItemList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			items: [],
			open: false,
			page: 0,
			pageSize: DEFAULT_PAGE_SIZE,
			pageResizeTo: DEFAULT_PAGE_SIZE,
			pagesCount: 0,
			loading: false
		};
	}

	get strings() {
		return {
			tableHeaderText: '',
			noItems: '',
			perPage: ''
		};
	}

	get icons() {
		return {
			pageResize: undefined
		};
	}

	// noinspection JSMethodCanBeStatic
	get pageSizeRange() {
		return PAGE_SIZE_RANGE;
	}

	get columnCount() {
		return 0;
	}

	// noinspection JSMethodCanBeStatic
	get tableProps() {
		return {celled: true, striped: true};
	}

	// noinspection JSMethodCanBeStatic
	async componentDidMount() {
		await this.updateTable();
	}

	async countItems() {
		throw new Error('not implemented');
	}

	async loadPage(page, pageSize) {
		throw new Error('not implemented');
	}

	renderItemRow(item) {
		throw new Error('not implemented');
	}

	renderTableHeader() {
		return <Fragment>
			<TableRow>
				<TableHeaderCell colSpan={this.columnCount - 1}>
					<span>{this.strings.tableHeaderText}</span>
				</TableHeaderCell>
				<TableHeaderCell collapsing>
					{this.renderHeaderControls()}
				</TableHeaderCell>
			</TableRow>
		</Fragment>;
	}

	renderHeaderControls() {
		throw new Error('not implemented');
	}

	async countPages(_pageSize) {
		let itemsCount = await this.countItems();
		let pageSize = _pageSize || this.state.pageSize;
		return Math.max(0, _.ceil(itemsCount / pageSize));
	}

	async updatePage(page) {
		await this.setState({page});
		await this.updateTable();
	}

	async updateTable() {
		let page = this.state.page;
		let pageSize = this.state.pageSize;

		await this.loading(true);
		await this.setState({
			items: await this.loadPage(page, pageSize),
			pagesCount: await this.countPages()
		});
		await this.loading(false);
	}

	async loading(loading) {
		await this.setState({loading});
	}

	renderTableRows() {
		let items = this.state.items || [];

		if (this.state.loading) {
			return <TableRow>
				<TableCell colSpan={this.columnCount}>
					<Divider hidden/>
					<Loader size={'large'} active inline={'centered'}/>
					<Divider hidden/>
				</TableCell>
			</TableRow>;
		}

		if (_.isEmpty(items)) {
			return <TableRow>
				<TableCell colSpan={this.columnCount}>
					<Header disabled
					        size={'small'}
					        textAlign={'center'}
					        className={'no items'}
					        content={this.strings.noItems}
					/>
				</TableCell>
			</TableRow>;
		}

		return items.map(item =>
			this.renderItemRow(item)
		);
	}

	async resizePage() {
		let pageSize = _.toInteger(this.state.pageResizeTo || this.state.pageSize);
		let {page} = this.state;
		let pagesCount = await this.countPages(pageSize);
		page = _.min([page, pagesCount - 1]);
		await this.setState({pageSize, page, pagesCount});
		await this.updateTable();
	}

	renderBody() {
		return <Table {...this.tableProps}>
			<TableHeader>
				{this.renderTableHeader()}
			</TableHeader>
			<TableBody>
				{this.renderTableRows()}
			</TableBody>
			<TableFooter>
				{this.renderTableFooter()}
			</TableFooter>
		</Table>;
	}

	renderTableFooter() {
		return <TableRow>
			<TableHeaderCell colSpan={7}>
				<FlexRow>
					<FlexCol grow={1} align={'center'}>
						<Pagination
							className={'no-shadow'}
							activePage={(this.state.page || 0) + 1}
							totalPages={this.state.pagesCount || 1}
							onPageChange={async (evt, {activePage}) => await this.updatePage(activePage - 1)}/>
					</FlexCol>
					<FlexCol grow={0} align={'center'}>
						<Popup inverted
						       size={'tiny'}
						       position={'left center'}
						       content={this.strings.perPage}
						       trigger={
							       <Input
								       type={'number'}
								       labelPosition={'right'}
								       value={this.state.pageResizeTo}
								       onKeyPress={async (event) => await this.onKeyPressResizePage(event)}
								       onChange={async (evt) => await this.setState({pageResizeTo: evt.target.value})}
								       label={
									       <Button icon={this.icons.pageResize}
									               onClick={async () => await this.resizePage()}/>
								       }
								       {...this.pageSizeRange}
							       />
						       }/>
					</FlexCol>
				</FlexRow>
			</TableHeaderCell>
		</TableRow>;
	}

	async onKeyPressResizePage(event) {
		let {char} = whatkey(event);
		if (char === '\n') {
			await this.resizePage();
		}
	}
}

//
