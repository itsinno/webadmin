/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React   from 'react';
import {Label} from 'semantic-ui-react';
import moment  from 'moment';

let DATE_TAG_FORMAT = 'DD MMMM YYYY, hh:mm';

export default class DateTag extends React.Component {
	get date() {
		return this.props.date || moment.unix();
	}

	formatUnixTime = (date) => moment.unix(date).format(DATE_TAG_FORMAT);

	render() {
		return <Label basic icon={'clock outline'} content={this.formatUnixTime(this.date)}/>;
	}
}
