/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}                             from 'react';
import {Dimmer, Header, Loader, Modal, ModalContent} from 'semantic-ui-react';
import {ItemList}                                    from './ItemList';

export class ItemListModal extends ItemList {
	constructor(props) {
		super(props);
		this.state.open = false;
	}

	render() {
		return <Fragment>
			<Modal open={this.state.open}
			       trigger={this.props.trigger}
			       onOpen={async () => await this.modalOpen(true)}
			       onClose={async () => await this.modalOpen(false)}>
				<Header icon={this.icons.modalHeader} content={this.strings.modalHeader}/>
				<ModalContent>
					<Dimmer inverted active={this.state.loading}>
						<Loader/>
					</Dimmer>
					{this.renderBody()}
				</ModalContent>
			</Modal>
		</Fragment>;
	}

	async modalOpen(open) {
		await this.setState({open});
	}

}
