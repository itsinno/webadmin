/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}             from 'react';
import {connect}                     from 'react-redux';
import {redux}                       from '../../utils/misc';
import {AC_LOGIN, AC_LOGOUT, Action} from '../../redux/actions';
import User                          from '../../api/auth/User';
import _                             from 'lodash';

class AuthVerifier extends React.Component {
	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get anonymous() {
		return this.user.isAnonymous;
	}

	get login() {
		return this.props.login || ($ => $);
	}

	get logout() {
		return this.props.login || ($ => $);
	}

	async componentDidMount() {
		if (this.anonymous) return;
		if (!this.user.token || !this.user.token.valid) {
			this.logout();
		} else if (this.user.token.tokenExpired) {
			await this.user.token.refresh();
			this.login(this.user);
		}
	}

	render = () => <Fragment/>;
}

export default connect(
	redux.with('auth'),
	{
		login: Action.for(AC_LOGIN).action,
		logout: Action.for(AC_LOGOUT).action
	}
)(AuthVerifier);
