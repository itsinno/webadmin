/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
import React                     from 'react';
import {Dropdown, Flag, Segment} from 'semantic-ui-react';
import {languageOption}          from '../../utils/misc';
import Config                    from '../../config/base';
import i18next                   from 'i18next';
import {promisify}               from 'util';

const LANGUAGES = Config.general.languages.map(lang => languageOption(lang));


const changeLang = promisify((...args) => i18next.changeLanguage(...args));

class LanguageSelector extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			lang: Config.general.defaultLanguage
		};
	}

	get flag() {
		return languageOption(this.state.lang).flag;
	}

	async componentDidMount() {
		await changeLang(this.state.lang);
	}

	async updateLang(lang) {
		await this.setState({lang});
		await changeLang(lang);
	}

	render() {
		return (
			<div className={'langsel-container'}>
				<Segment raised>
					<Flag name={this.flag}/>
					<Dropdown
						inline
						options={LANGUAGES}
						value={this.state.lang}
						onChange={async (evt, {value: lang}) =>
							await this.updateLang(lang)
						}
					/>
				</Segment>
			</div>
		);
	}
}

export default LanguageSelector;
