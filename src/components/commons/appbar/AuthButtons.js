/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                           from 'react';
import {Button, ButtonGroup, MenuItem} from 'semantic-ui-react';
import {Link, withRouter}              from 'react-router-dom';
import {buildLocation}                 from '../../../utils/misc';
import { withTranslation } from 'react-i18next';


class AuthButtons extends React.Component {

	get from() {
		return buildLocation(this.props.location || {});
	}

	get t() {
		return this.props.t;
	}

	render() {
		return (
			<MenuItem position={'right'}>
				<ButtonGroup>
					<Button as={Link} to={{pathname: '/sign-in', search: `?from=${this.from}`}} content={this.t('sign-in')/*'Sign in'*/}
					        labelPosition={'left'}
					        icon={'sign in alternate'} primary/>
				</ButtonGroup>
			</MenuItem>
		);
	}
}

export default withRouter(withTranslation('commons/AppBar/AuthButtons')(AuthButtons));
