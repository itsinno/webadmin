/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React       from 'react';
import UserMenu    from './UserMenu';
import {connect}   from 'react-redux';
import {redux}     from '../../../utils/misc';
import User        from '../../../api/auth/User';
import AuthButtons from './AuthButtons';
import _           from 'lodash';


class AuthMenuGroup extends React.Component {
	/**
	 * @return {User}
	 */
	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	render() {
		return this.user.isAnonymous() ?
			<AuthButtons/> :
			<UserMenu/>;
	}
}


export default connect(
	redux.with('auth')
)(AuthMenuGroup);
