/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                                      from 'lodash';
import React, {Fragment}                      from 'react';
import {Dropdown, DropdownItem, DropdownMenu} from 'semantic-ui-react';
import UserView                               from '../UserView';
import {connect}                              from 'react-redux';
import {redux}                                from '../../../utils/misc';
import {AC_LOGOUT, Action}                    from '../../../redux/actions';
import User                                   from '../../../api/auth/User';
import { withTranslation } from 'react-i18next';

class UserMenu extends React.Component {
	
	get t() {
		return this.props.t;
	}
	
	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	render() {
		return <Fragment>
			<Dropdown trigger={<UserView user={this.user}/>} item className={'right'}>
				<DropdownMenu>
					<DropdownItem onClick={async () => await this.logout()}>
						{this.t('log-out')/*Log out*/}
					</DropdownItem>
				</DropdownMenu>
			</Dropdown>
		</Fragment>;
	}

	async logout() {
		await this.props.logout();
	}
}

export default connect(
	redux.with('auth'),
	{logout: Action.for(AC_LOGOUT).action}
)(withTranslation('commons/AppBar/UserMenu')(UserMenu));
