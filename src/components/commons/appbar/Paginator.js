/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, { Fragment } from 'react';
import _                   from 'lodash';
import { withTranslation } from 'react-i18next';
import Authority           from '../../../api/auth/Authority';
import { UID }             from '../../../utils/misc';
import { MenuItem }        from 'semantic-ui-react';
import { withRouter }      from 'react-router-dom';
import Authorized          from '../Authorized';
import User                from '../../../api/auth/User';

const PAGES = (t) => [
	{ name: t('statistics'), link: '/stats', authority: Authority.anonymous },
	{ name: t('knowledge-base'), link: '/knowledge-base', authority: Authority.user },
	{ name: t('connected-bots'), link: '/bots', authority: Authority.engineer },
	{ name: t('web-terminal'), link: '/terminal', authority: Authority.admin },
	{ name: t('access-manager'), link: '/access-manager', authority: Authority.admin }
];

class Paginator extends React.Component {
	get pages() {
		return PAGES(this.t);
	}

	get t() {
		return this.props.t || ($ => $);
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get location() {
		return this.props.location || {};
	}

	get history() {
		return this.props.history || {};
	}

	render() {
		return <Fragment>
			{_.map(this.pages, (page) => this.renderPageTrigger(page))}
		</Fragment>;
	}

	isPageActive = ({ link }) => this.location.pathname.startsWith(link);

	renderPageTrigger(page) {
		return <Authorized prop={'disabled'} on={'deny'} authority={page.authority} key={UID(page.link)}>
			<MenuItem content={page.name}
			          active={this.isPageActive(page)}
			          onClick={async () => await this.pageChanged(page)}
			/>
		</Authorized>;
	}

	async pageChanged({ link }) {
		this.history.push(link);
	}
}

export default withRouter(
	withTranslation('commons/AppBar/Paginator')(Paginator)
);
