/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}          from 'react';
import {Divider, Header, Segment} from 'semantic-ui-react';

export default class NamedSegment extends React.Component {
	render() {
		let value = this.props.title;
		let body = this.props.children || <Fragment/>;
		let props = {...this.props, title: undefined, children: undefined};

		return <Segment {...props}>
			{value ?
				<Fragment>
					<Header size={'tiny'} content={value}/>
					{this.props.children ? <Divider/> : <Fragment/>}
				</Fragment> :
				<Fragment/>}
			{body}
		</Segment>;
	}
}
