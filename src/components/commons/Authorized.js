/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment} from 'react';
import {redux}           from '../../utils/misc';
import {connect}         from 'react-redux';
import User              from '../../api/auth/User';
import Authority         from '../../api/auth/Authority';
import _                 from 'lodash';

class Authorized extends React.Component {
	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get active() {
		switch (this.on) {
		case 'deny':
			return !this.grant(this.authority);
		case 'allow':
		default:
			return this.grant(this.authority);
		}
	}

	get on() {
		return this.props.on || 'allow';
	}

	get exact() {
		return this.props.exact || false;
	}

	get authority() {
		return this.props.authority || Authority.anonymous;
	}

	get childProps() {
		let props = _.cloneDeep(this.props);

		delete props.render;
		delete props.prop;
		delete props.authority;
		delete props.children;
		delete props.dispatch;

		return this.setProps(props);
	}

	get children() {
		if (this.props.render) {
			return this.props.children;
		} else {
			let child = React.Children.only(this.props.children);
			return React.cloneElement(child, {...child.props, ...this.childProps});
		}
	}

	grant() {
		const auth = Authority.of(this.user);

		return this.exact ?
			auth.exactly(this.authority) :
			auth.granted(this.authority);
	}

	setProps(props) {
		if (_.isString(this.props.prop)) {
			return {
				...props,
				[this.props.prop]: this.active
			}
		} else if (_.isObject(this.props.prop)) {
			return this.active ? this.props.prop : {};
		} else {
			return props;
		}
	}

	mustRender() {
		return !this.props.render || this.active;
	}

	render() {
		return this.mustRender() ? this.children : <Fragment/>;
	}
}

export default connect(
	redux.with('auth')
)(Authorized);
