/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}       from 'react';
import {Image, Menu, MenuItem} from 'semantic-ui-react';
import AuthMenuGroup           from './appbar/AuthMenuGroup';
import Paginator               from './appbar/Paginator';
import {Link, withRouter}      from 'react-router-dom';

const APPBAR_INVERTED = true;

class AppBar extends React.Component {
	render() {
		return <Fragment>
			<Menu inverted={APPBAR_INVERTED} fixed={'top'}>
				<MenuItem header>
					<Link to='/'>
						<Image
							avatar
							alt={'Inno'}
							src={'https://university.innopolis.ru/bitrix/templates/.default/static/img/other-images/firsthand-logo.jpg'}
						/>
					</Link>
				</MenuItem>
				<Paginator/>
				<AuthMenuGroup/>
			</Menu>
		</Fragment>;
	}
}

export default withRouter(AppBar);
