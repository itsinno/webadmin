/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}             from 'react';
import Authorized                    from './Authorized';
import Authority                     from '../../api/auth/Authority';
import {Redirect, Route, withRouter} from 'react-router-dom';
import {buildLocation}               from '../../utils/misc';

class RestrictedRoute extends React.Component {
	get here() {
		return buildLocation(this.props.location || {});
	}

	render() {
		return <Fragment>
			<Authorized render on={'deny'} exact authority={Authority.anonymous}>
				<Route {...this.props} />
			</Authorized>
			<Authorized render on={'allow'} exact authority={Authority.anonymous}>
				<Route exact={this.props.exact} path={this.props.path} component={() => this.renderRedirect()}/>
			</Authorized>
		</Fragment>;
	}

	renderRedirect() {
		return <Redirect to={{
			pathname: '/sign-in',
			search: `?from=${this.here}`
		}}/>;
	}
}

export default withRouter(RestrictedRoute);