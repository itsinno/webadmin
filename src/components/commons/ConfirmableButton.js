/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _               from 'lodash';
import React           from 'react';
import {Button, Popup} from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class ConfirmableButton extends React.Component {

	BUTTON_PROPS = {
		content: this.t('confirm')/*'Confirm'*/,
		icon: 'checkmark',
		primary: true,
		labelPosition: 'left'
	};

	constructor(props) {
		super(props);
		this.state = {
			loading: false
		};
	}

	get t() {
		return this.props.t;
	}

	get buttonProps() {
		let buttonProps = this.props.buttonProps || {};
		return {...this.BUTTON_PROPS, ...buttonProps};
	}

	get passedProps() {
		let props = _.clone(this.props);

		delete props.onConfirm;
		delete props.buttonProps;

		return props;
	}

	get onConfirmed() {
		return _.isFunction(this.props.onConfirmed) ? this.props.onConfirmed : value => value;
	}

	async confirm() {
		await this.setState({loading: true});
		await this.onConfirmed();
		await this.setState({loading: false});
	}

	render() {
		return (
			<Popup trigger={this.props.trigger} {...this.passedProps} on={'click'}>
				<Button {...this.buttonProps}
				        loading={this.state.loading}
				        onClick={async () => await this.confirm()}
				/>
			</Popup>
		);
	}
}

export default withTranslation('commons/ConfirmableButton')(ConfirmableButton);