/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _             from 'lodash';
import React         from 'react';
import {Icon, Input} from 'semantic-ui-react';
import whatkey       from 'whatkey';
import { withTranslation } from 'react-i18next';

class SearchInput extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			value: ''
		};
	}

	get t() {
		return this.props.t;
	}

	get passedProps() {
		let props = _.clone(this.props);

		delete props.onSearch;

		return props;
	}

	async reset() {
		await this.setState({value: this.props.value || ''});
	}

	async componentDidMount() {
		await this.reset();
	}

	async componentDidUpdate(prevProps, prevState, snapshot) {
		if (_.isEqual(prevProps, this.props)) return;
		await this.reset();
	}

	render() {
		return <Input
			{...this.passedProps}
			placeholder={this.t('search')/*'Search...'*/}
			value={this.state.value}
			onChange={async ({target}) => await this.setValue(target.value)}
			onKeyPress={async (event) => await this.onKeyPressTriggerChange(event)}
			icon={
				<Icon
					link
					name='search'
					onClick={async () => await this.changed()}
				/>
			}
		/>;
	}

	onSearch = () => this.props.onSearch || (async value => value);

	async changed() {
		this.onSearch()(this.state.value);
	}

	async onKeyPressTriggerChange(event) {
		let {char} = whatkey(event);
		if (char === '\n') {
			await this.changed();
		}
	}

	async setValue(value) {
		await this.setState({value});
	}
}

export default withTranslation('commons/SearchInput')(SearchInput);
