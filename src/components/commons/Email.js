/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React   from 'react';
import {Label} from 'semantic-ui-react';

class Email extends React.Component {
	render() {
		return <Label as={'a'}
		              icon={'mail'}
		              size={'large'}
		              content={this.props.email}
		              href={`mailto:${this.props.email}`}
		/>;
	}
}

export default Email;