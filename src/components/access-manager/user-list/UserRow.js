/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                 from 'react';
import {TableCell, TableRow} from 'semantic-ui-react';
import Email                 from '../../commons/Email';
import AuthorityEditor       from './user-row/AuthorityEditor';

class UserRow extends React.Component {
	/**
	 *
	 * @return {User}
	 */
	get user() {
		return this.props.user || {};
	}

	onChange = () => this.props.onChange || ($ => $);
	onSave = () => this.props.onSave || ($ => $);

	render() {
		return (
			<TableRow>
				<TableCell collapsing content={this.user.name}/>
				<TableCell>
					<Email email={this.user.email}/>
				</TableCell>
				<TableCell collapsing content={this.renderAuthority()}/>
			</TableRow>
		);
	}

	renderAuthority() {
		return <AuthorityEditor authority={this.user.authority}
		                        disabled={this.props.editorDisabled}
		                        onChange={async (value) => await this.changeAuthority(value)}
		                        onSave={async () => await this.onSave()()}
		/>;
	}

	async changeAuthority(authority) {
		await this.onChange()({
			...this.user,
			authority
		});
	}
}

export default UserRow;
