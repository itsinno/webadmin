/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                  from 'react';
import _                      from 'lodash';
import {UID}                  from '../../../../utils/misc';
import User                   from '../../../../api/auth/User';
import ApiResolver            from '../../../../api/ApiResolver';
import {Button, Icon, Select} from 'semantic-ui-react';
import {Authorities}          from '../../../../api/auth/Authority';
import { withTranslation } from 'react-i18next';

class AuthorityEditor extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false
		};
	}

	get t() {
		return this.props.t;
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).users;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast || ($ => $),
			closeToast: this.props.closeToast || ($ => $)
		};
	}

	get options() {
		return Authorities
			.filter((authority) => authority.level > 0)
			.map((authority) => ({
				key: UID(authority.name),
				value: authority.level,
				icon: <Icon name={authority.icon} color={authority.color}/>,
				text: authority.name
			}));
	}

	render() {
		return <>
			<Select
				disabled={this.props.disabled}
				search options={this.options}
				value={this.props.authority}
				onChange={async (e, {value}) => await this.onChange()(value)}
			/>
			<Button
				disabled={this.props.disabled}
				style={{marginLeft: 8}}
				loading={this.state.loading}
				color={'green'}
				content={this.t('save')/*'Save'*/}
				icon={'save'}
				labelPosition={'right'}
				onClick={async () => await this.save()}
			/>
		</>;
	}

	onChange = () => this.props.onChange || ($ => $);
	onSave = () => this.props.onSave || ($ => $);

	async save() {
		await this.setState({loading: true});
		await this.onSave()();
		await this.setState({loading: false});
	}
}

// export default connect(
// 	redux.with('auth'),
// 	redux.withToaster()
// )(AuthorityEditor);

export default withTranslation('AccessManager/components/UserList/UserRow/AuthorityEditor')(AuthorityEditor);