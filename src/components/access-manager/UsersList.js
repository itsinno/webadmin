/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}                   from 'react';
import {ItemList}                          from '../commons/ItemList';
import _                                   from 'lodash';
import ApiResolver                         from '../../api/ApiResolver';
import SearchInput                         from '../commons/SearchInput';
import UserRow                             from './user-list/UserRow';
import User                                from '../../api/auth/User';
import {redux, UID}                        from '../../utils/misc';
import {connect}                           from 'react-redux';
import {Button, TableHeaderCell, TableRow} from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class UsersList extends ItemList {
	render = this.renderBody;

	constructor(props) {
		super(props);
		this.state = {
			...this.state,
			query: '',
			saving: false,
			unsaved: {}
		};
	}

	get t() {
		return this.props.t;
	}

	get strings() {
		return {
			noItems: this.t('no-users')/*'There are no users available'*/,
			perPage: this.t('users-per-page')/*'Users per page'*/,
			tableHeaderText: this.t('registered-users') /*'Registered users'*/
		};
	}

	get columnCount() {
		return 3;
	}

	get icons() {
		return {
			pageResize: 'expand arrows alternate'
		};
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast || ($ => $),
			closeToast: this.props.closeToast || ($ => $)
		};
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).users;
	}

	get query() {
		// nulling search query due to API restrictions
		return this.state.query || null;
	}

	async searchUpdated(query) {
		await this.setState({query});
		await this.updateTable();
	}

	renderHeaderControls() {
		return <Fragment>
			<SearchInput
				fluid
				value={this.state.query}
				disabled={this.state.loading}
				loading={this.query && this.state.loading}
				onSearch={async (value) => await this.searchUpdated(value)}/>
		</Fragment>;
	}

	async loadPage(page, pageSize) {
		await this.setState({unsaved: {}});

		return await this.api.getUsers({search: this.query, page, pageSize});
	}

	async countItems() {
		return await this.api.countUsers({search: this.query});
	}

	renderTableHeader() {
		return <Fragment>
			<TableRow>
				<TableHeaderCell colSpan={this.columnCount - 1}>
					{this.renderHeaderControls()}
				</TableHeaderCell>
				<TableHeaderCell collapsing>
					{this.renderSaveAll()}
				</TableHeaderCell>
			</TableRow>
		</Fragment>;
	}

	renderItemRow(item) {
		const unsaved = this.state.unsaved[item.email];

		if (unsaved) item = unsaved;

		return <UserRow
			editorDisabled={this.state.saving}
			key={UID(item.email)}
			user={item}
			onChange={async (value) => await this.itemChanged(value)}
			onSave={async () => this.save(item.email)}
		/>;
	}

	renderSaveAll() {
		return <Button fluid
		               loading={this.state.saving}
		               color={'green'}
		               icon={'save'}
		               content={this.t('save-all')/*'Save All'*/}
		               onClick={async () => await this.saveAll()}
		/>;
	}

	async save(email) {
		const unsavedItem = this.state.unsaved[email];
		if (!unsavedItem) return;

		const updated = await this.api.setAuthority({
			email,
			authority: unsavedItem.authority
		});

		await this.resetUnsaved(email, updated);
	}

	async saveAll() {
		await this.setState({saving: true});

		const unsaved = Object.keys(this.state.unsaved);
		const promises = unsaved.map((email) => this.save(email));

		await Promise.all(promises);

		await this.setState({saving: false});
	}

	async itemChanged(item) {
		await this.setState({
			unsaved: {
				...this.state.unsaved,
				[item.email]: item
			}
		});
	}

	async resetUnsaved(email, updated) {
		let {unsaved, items} = this.state;

		items = items.map((item) => item.email !== email ? item : updated || item);
		delete unsaved[email];

		await this.setState({unsaved, items});
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('AccessManager/components/UsersList')(UsersList));
