/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment} from 'react';
import {Button, Popup}   from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class BotListControls extends React.Component {

	get t() {
		return this.props.t;
	}

	render() {
		return (
			<Fragment>
				<Popup inverted
				       size={'tiny'}
				       trigger={
					       <Button icon={'plus'}
					               color={'green'}
					               floated={'right'}
					               content={this.t('new-bot')/*'New bot'*/}
					               labelPosition={'left'}
					       />
				       }
				>
					This action is not supported by current bundle
				</Popup>
			</Fragment>
		);
	}
}

export default withTranslation('Bots/components/BotList/BotListControls')(BotListControls);
