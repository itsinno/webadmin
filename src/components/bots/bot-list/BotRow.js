/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, { Fragment }                  from 'react';
import { Icon, Label, TableCell, TableRow } from 'semantic-ui-react';
import BotRowControls                       from './BotRowControls';

const BOT_TYPES = {
	telegramBot: {
		name: 'Telegram',
		icon: { name: 'telegram', color: 'blue' }
	},
	vkBot: {
		name: 'VKontakte',
		icon: { name: 'vk', color: 'violet' }
	},
	whatsappBot: {
		name: 'WhatsApp',
		icon: { name: 'whatsapp', color: 'green' }
	}
};
//
// const BOT_STATUSES = {
// 	alive: {
// 		name: 'Running',
// 		icon: {name: 'play', color: 'green'}
// 	},
// 	dead: {
// 		name: 'Stopped',
// 		icon: {name: 'stop', color: 'red'}
// 	},
// 	unknown: {
// 		name: 'Unknown',
// 		icon: {name: 'question circle', color: 'grey'}
// 	}
// };

class BotRow extends React.Component {
	get bot() {
		return this.props.bot || {};
	}

	render() {
		return (
			<TableRow>
				<TableCell content={this.renderType()}/>
				<TableCell textAlign={'center'} content={this.renderVersion()}/>
				<TableCell textAlign={'center'} content={this.renderReadonly()}/>
				<TableCell collapsing>
					<BotRowControls bot={this.bot} onChange={this.props.onChange}/>
				</TableCell>
			</TableRow>
		);
	}

	// renderStatus() {
	// 	let status = BOT_STATUSES[this.bot.status] || BOT_STATUSES['unknown'];
	//
	// 	return <Fragment>
	// 		<Icon {...status.icon} />
	// 		<span>{status.name}</span>
	// 	</Fragment>;
	// }

	renderType() {
		let type = BOT_TYPES[this.bot.messenger] || {};
		return <Fragment>
			<Icon {...type.icon} />
			<span>{type.name}</span>
		</Fragment>;
	}

	renderReadonly() {
		return this.bot.acceptingMessages ? <Fragment>
			<Icon name={'conversation'} color={'olive'}/> <span>Accepting messages</span>
		</Fragment> : <Fragment>
			<Icon name={'bullhorn'} color={'orange'}/> <span>Ignoring messages</span>
		</Fragment>;
	}

	renderVersion() {
		return <Label basic>
			{this.bot.version || 'latest'}
		</Label>;
	}
}

export default BotRow;
