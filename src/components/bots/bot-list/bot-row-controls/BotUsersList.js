/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _               from 'lodash';
import React, {Fragment}           from 'react';
import {ItemListModal} from '../../../commons/ItemListModal';
import {connect}       from 'react-redux';
import {redux, UID}         from '../../../../utils/misc';
import User            from '../../../../api/auth/User';
import ApiResolver     from '../../../../api/ApiResolver';
import {Button, Icon}        from 'semantic-ui-react';
import BotUserRow      from './bot-users-list/BotUserRow';
import { withTranslation } from 'react-i18next';

const BOT_TYPES = {
	telegramBot: {
		name: 'Telegram',
		icon: {name: 'telegram', color: 'blue'}
	},
	vkBot: {
		name: 'VKontakte',
		icon: {name: 'vk', color: 'violet'}
	},
	whatsappBot: {
		name: 'WhatsApp',
		icon: {name: 'whatsapp', color: 'green'}
	}
};

class BotUsersList extends ItemListModal {

	get t() {
		return this.props.t;
	}

	get columnCount() {
		return 4;
	}

	get icons() {
		return {
			modalHeader: 'users',
			pageResize: 'expand arrows alternate'
		}
	}

	get strings() {
		return {
			tableHeaderText: this.renderType(),
			noItems: this.t('no-users')/*'No users registered'*/,
			perPage: this.t('users-per-page')/*'Users per page'*/,
			modalHeader: this.t('users-in-bot')/*'Users registered in bot'*/	
		};
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).bots;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast || ($ => $),
			closeToast: this.props.closeToast || ($ => $),
		};
	}

	get bot() {
		return this.props.bot || {};
	}

	renderItemRow(item) {
		return <BotUserRow key={UID(item.email)} user={item} onUpdated={async () => await this.updateTable()}/>;
	}

	renderHeaderControls() {
		return <Button as={'a'} href={this.bot.link} content={this.t('open-bot')/*'Open bot'*/} icon={'external alternate'} color={'green'}/>;
	}

	async loadPage(page, pageSize) {
		const {messenger} = this.bot;

		const data = await this.api.getBotUsers({messenger, page, pageSize});

		return data || [];
	}

	async countItems() {
		const {messenger} = this.bot;
		return _.get(await this.api.countBotUsers({messenger}), 'count', 0);
	}

	renderType() {
		let type = BOT_TYPES[this.bot.messenger] || {};
		return <Fragment>
			<Icon {...type.icon} />
			<span>{type.name}</span>
		</Fragment>;
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('Bots/components/BotList/BotRowControls/BotUsersList')(BotUsersList));