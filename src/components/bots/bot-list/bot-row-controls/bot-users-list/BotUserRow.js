/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                                   from 'lodash';
import React                               from 'react';
import {Button, Flag, TableCell, TableRow} from 'semantic-ui-react';
import {languageOption, redux, UID}        from '../../../../../utils/misc';
import ConfirmableButton                   from '../../../../commons/ConfirmableButton';
import {connect}                           from 'react-redux';
import ApiResolver                         from '../../../../../api/ApiResolver';
import User                                from '../../../../../api/auth/User';
import Email                               from '../../../../commons/Email';
import { withTranslation } from 'react-i18next';


class BotUserRow extends React.Component {
	
	get t() {
		return this.props.t;
	}
	
	get key() {
		const keyBase = `${this.botUser.messenger}:${this.botUser.userID}`;

		return UID(keyBase);
	}

	get botUser() {
		return this.props.user || {};
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).bots;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast || ($ => $),
			closeToast: this.props.closeToast || ($ => $),
		};
	}

	renderLang() {
		const lang = languageOption(this.botUser.language.toLowerCase());

		return <>
			<Flag name={lang.flag}/>&nbsp;{lang.text}
		</>;
	}

	async logoutUser(email) {
		await this.api.dropBotUser({email});
		await this.onUpdated()();
	}

	onUpdated = () => this.props.onUpdated || ($ => $);

	render() {
		return (
			<TableRow key={this.key}>
				<TableCell collapsing>
					{this.botUser.userID}
				</TableCell>
				<TableCell>
					<Email email={this.botUser.email}/>
				</TableCell>
				<TableCell collapsing>
					{this.renderLang()}
				</TableCell>
				<TableCell collapsing>
					<ConfirmableButton position={'bottom center'}
					                   trigger={<Button fluid
					                                    icon={'remove user'}
					                                    color={'red'}
					                                    content={this.t('logout')/*'Logout'*/}/>}
					                   onConfirmed={async () => await this.logoutUser(this.botUser.email)}
					                   buttonProps={{
						                   primary: false,
						                   color: 'red',
						                   icon: 'remove user',
						                   content: this.t('confirm')/*'Confirm'*/
					                   }}/>
				</TableCell>
			</TableRow>
		);
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('Bots/components/BotList/BotRowControls/BotUsersList/BotUserRow')(BotUserRow));