/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                              from 'lodash';
import React, { Fragment }            from 'react';
import { connect }                    from 'react-redux';
import { Button, ButtonGroup, Popup } from 'semantic-ui-react';
import ApiResolver                    from '../../../api/ApiResolver';
import User                           from '../../../api/auth/User';
import { redux }                      from '../../../utils/misc';
import ConfirmableButton              from '../../commons/ConfirmableButton';
import BotUsersList                   from './bot-row-controls/BotUsersList';
import { withTranslation }            from 'react-i18next';

class BotRowControls extends React.Component {
	get t() {
		return this.props.t;
	}

	get bot() {
		return this.props.bot || {};
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).bots;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: (...args) => this.props.openToast(...args),
			closeToast: (...args) => this.props.closeToast(...args)
		};
	}

	get onChange() {
		return this.props.onChange || ($ => $);
	}

	async stopBot(messenger) {
		await this.api.stopBot({ messenger });
		await this.onChange();
	}

	async startBot(messenger) {
		await this.api.startBot({ messenger });
		await this.onChange();
	}

	async restartBot(messenger) {
		await this.api.stopBot({ messenger });
		await this.api.startBot({ messenger });
		await this.onChange();
	}

	render() {
		return (
			<Fragment>
				<ButtonGroup basic floated={'right'}>
					<ConfirmableButton
						position={'bottom center'}
						trigger={<Button icon={'play'} disabled={!this.can('start')}/>}
						buttonProps={{
							primary: false,
							color: 'green',
							icon: 'play',
							content: this.t('start') /*'Start'*/
						}}
						onConfirmed={async () => this.startBot(this.bot.messenger)}
					/>
					<ConfirmableButton
						position={'bottom center'}
						trigger={<Button icon={'redo'} disabled={!this.can('restart')}/>}
						buttonProps={{
							primary: false,
							color: 'olive',
							icon: 'redo',
							content: this.t('restart') /*'Restart'*/
						}}
						onConfirmed={async () => this.restartBot(this.bot.messenger)}
					/>
					<ConfirmableButton
						position={'bottom center'}
						trigger={<Button icon={'stop'} disabled={!this.can('stop')}/>}
						buttonProps={{
							primary: false,
							color: 'orange',
							icon: 'stop',
							content: this.t('stop') /*'Stop'*/
						}}
						onConfirmed={async () => this.stopBot(this.bot.messenger)}
					/>
					<BotUsersList
						bot={this.bot}
						trigger={<Button icon={'users'} disabled={this.props.disabled}/>}
					/>
					<Popup
						inverted
						size={'tiny'}
						trigger={
							<Button
								color={'red'}
								icon={'trash'}
								disabled={!this.can('remove')}
							/>
						}
					>
						{this.t(
							'action-not-supported'
						) /*This action is not supported by current bundle*/}
					</Popup>
				</ButtonGroup>
			</Fragment>
		);
	}

	can(action) {
		switch (action) {
		case 'start':
			return !this.bot.acceptingMessages;
		case 'restart':
		case 'stop':
			return this.bot.acceptingMessages;
		default:
			return true;
		}
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('Bots/components/BotList/BotRowControls')(BotRowControls));
