/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, { Fragment } from 'react';
import {
	Divider,
	Header,
	Loader,
	Table,
	TableBody,
	TableCell,
	TableFooter,
	TableHeader,
	TableHeaderCell,
	TableRow
}                          from 'semantic-ui-react';
import _                   from 'lodash';
import BotListControls     from './bot-list/BotListControls';
import BotRow              from './bot-list/BotRow';
import { connect }         from 'react-redux';
import { redux, UID }      from '../../utils/misc';
import ApiResolver         from '../../api/ApiResolver';
import User                from '../../api/auth/User';
import { withTranslation } from 'react-i18next';


class BotList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			bots: [],
			loading: false
		};
	}

	get t() {
		return this.props.t;
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).bots;
	}

	get strings() {
		return {
			noItems: this.t('no-bots')/*'No bots configured'*/
		};
	}

	get bots() {
		return this.state.bots || [];
	}

	get columnCount() {
		return 5;
	}

	get toaster() {
		return {
			openToast: this.props.openToast || ($ => $),
			closeToast: this.props.closeToast || ($ => $)
		};
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	async componentDidMount() {
		await this.setState({ loading: true });
		await this.loadTable();
		await this.setState({ loading: false });
	}

	render() {
		return <Fragment>
			<Table definition celled striped>
				<TableHeader>
					<TableRow>
						<TableHeaderCell/>
						<TableHeaderCell colSpan={this.columnCount - 1}>
							<BotListControls/>
						</TableHeaderCell>
					</TableRow>
				</TableHeader>
				<TableBody>
					{this.renderTableRows()}
				</TableBody>
				<TableFooter>
				</TableFooter>
			</Table>
		</Fragment>;
	}

	renderTableRows() {
		if (this.state.loading) {
			return <TableRow>
				<TableCell colSpan={this.columnCount}>
					<Divider hidden/>
					<Loader size={'large'} active inline={'centered'}/>
					<Divider hidden/>
				</TableCell>
			</TableRow>;
		}

		if (_.isEmpty(this.bots)) {
			return <TableRow>
				<TableCell colSpan={this.columnCount}>
					<Header disabled
					        size={'small'}
					        textAlign={'center'}
					        className={'no items'}
					        content={this.strings.noItems}
					/>
				</TableCell>
			</TableRow>;
		}

		return this.bots.map(bot => <BotRow key={UID(bot.messenger)}
		                                    onChange={async () => await this.loadTable()}
		                                    bot={bot}/>);
	}

	async loadTable() {
		const bots = await this.api.getBots() || [];
		if (!bots) return;

		await this.setState({ bots });
	}

}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('Bots/components/BotList')(BotList));