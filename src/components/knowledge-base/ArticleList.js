/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                                  from 'lodash';
import React, {Fragment}                  from 'react';
import {ItemList}                         from '../commons/ItemList';
import {Divider, Header, TableHeaderCell} from 'semantic-ui-react';
import ApiResolver                        from '../../api/ApiResolver';
import ArticleRow                         from './article-list/ArticleRow';
import SearchInput                        from '../commons/SearchInput';
import TableRow                           from 'semantic-ui-react/dist/commonjs/collections/Table/TableRow';
import FlexRow                            from '../../utils/components/flex/FlexRow';
import FlexCol                            from '../../utils/components/flex/FlexCol';
import {searchUrl}                        from './commons/misc';
import ArticleListControls                from './article-list/ArticleListControls';
import {withRouter}                       from 'react-router-dom';
import SearchParser                       from '../../utils/knowledge-base/SearchParser';
import {redux}                            from '../../utils/misc';
import {connect}                          from 'react-redux';
import User                               from '../../api/auth/User';
import { withTranslation } from 'react-i18next';

class ArticleList extends ItemList {

	constructor(props) {
		super(props);
		this.state.searchQuery = '';
	}

	get t() {
		return this.props.t;
	}

	get strings() {
		return {
			noItems: this.t('no-articles')/*'No articles present'*/,
			perPage: this.t('articles-per-age')/*'Articles per page'*/
		};
	}

	get columnCount() {
		return 5;
	}

	get icons() {
		return {
			pageResize: 'expand arrows alternate'
		};
	}

	get isSearching() {
		return !_.isEmpty(this.state.searchQuery);
	}

	get history() {
		return this.props.history || {push: value => value};
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).knowledgeBase;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: (toast) => this.props.openToast(toast),
			closeToast: (toastId) => this.props.closeToast(toastId),
		}
	}

	async loadSearchPage(query, page, pageSize) {
		query = SearchParser.parse(query);
		return await this.api.searchArticles({query, page, pageSize});
	}

	async countFoundItems(query) {
		query = SearchParser.parse(query);
		return await this.api.countFoundArticles({query});
	}

	render() {
		return <Fragment>
			<FlexRow>
				<FlexCol grow={1} align={'center'}>
					<Header content={this.t('knowledge-base')/*'Knowledge base'*/}/>
				</FlexCol>
				<FlexCol grow={0} align={'center'}>
					<ArticleListControls/>
				</FlexCol>
			</FlexRow>
			<Divider/>
			{this.renderBody()}
		</Fragment>;
	} ;

	async componentDidMount() {
		await this.setState({searchQuery: this.props.query});
		await super.componentDidMount();
	}

	async componentDidUpdate(prevProps, prevState, snapshot) {
		if (_.isEqual(prevProps, this.props)) return;
		await this.setState({searchQuery: this.props.query});
		await this.updateTable();
	}

	async loadPage(page, pageSize) {
		const data = await this.loadPageInternal(page, pageSize);

		return _.isArray(data) ? data : [];
	}

	async loadPageInternal(page, pageSize) {
		if (this.isSearching)
			return await this.loadSearchPage(this.state.searchQuery, page, pageSize);
		else
			return await this.api.getArticles({page, pageSize});
	}

	async countItems() {
		if (this.isSearching)
			return await this.countFoundItems(this.state.searchQuery);
		else
			return await this.api.countArticles({});
	}

	renderItemRow(item) {
		return <ArticleRow
			key={item.id}
			article={item}
			onChange={async () => await this.updateTable()}
		/>;
	}

	renderTableHeader() {
		return <TableRow>
			<TableHeaderCell colSpan={this.columnCount}>
				<FlexRow>
					<FlexCol grow={1}/>
					<FlexCol grow={0}>
						<SearchInput
							value={this.props.query}
							disabled={this.state.loading}
							loading={this.isSearching && this.state.loading}
							onSearch={async (value) => await this.searchUpdated(value)}/>
					</FlexCol>
				</FlexRow>
			</TableHeaderCell>
		</TableRow>;
	}

	async searchUpdated(searchQuery) {
		await this.loading(true);
		this.history.push(searchUrl(searchQuery));
		await this.updatePage(0);
		await this.loading(false);
	}

}

export default withRouter(connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('KnowledgeBase/components/ArticleList')(ArticleList)));
