/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                   from 'react';
import _                       from 'lodash';
import {
	Accordion,
	AccordionContent,
	AccordionTitle,
	Button,
	Form,
	FormDropdown,
	FormField,
	FormInput,
	Header,
	Modal,
	ModalActions,
	ModalContent,
	Popup
}                              from 'semantic-ui-react';
import ApiResolver             from '../../../api/ApiResolver';
import TextareaAutosize        from 'react-textarea-autosize';
import Config                  from '../../../config/base';
import '../../../utils/ace-editor';
import {dropdownOption, redux} from '../../../utils/misc';
import {Editor}                from '@tinymce/tinymce-react';
import '../../../utils/tinymce-editor';
import {connect}               from 'react-redux';
import User                    from '../../../api/auth/User';
import { withTranslation } from 'react-i18next';

const TINYMCE_CONFIG = Config.knowledgeBase.articleEditor.mceConfig;

export class ArticleEditor extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			article: {},
			open: false,
			loading: false,
			sections: {
				common: true,
				code: true
			},
			tagVariants: []
		};
	}

	get t() {
		return this.props.t;
	}

	get tagVariants() {
		return (this.article.tags || []).map(value => dropdownOption(value));
	}

	get article() {
		return this.state.article;
	}

	get tags() {
		return this.article.tags || [];
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).knowledgeBase;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast,
		}
	}

	async componentDidMount() {
		await this.resetForm();
	}

	async resetForm() {
		let {article} = _.cloneDeep(this.props);
		await this.setState({article});
	}

	render() {
		return <Modal open={this.state.open}
		              trigger={this.props.trigger}
		              onOpen={this.modalOpen(true)}
		              onClose={this.modalOpen(false)}>
			{this.renderTitle()}
			<ModalContent>
				{this.renderBody()}
			</ModalContent>
			<ModalActions>
				<Popup content={this.t('reset-form')/*'Reset form'*/}
				       disabled={this.state.loading}
				       trigger={<Button icon={'ban'} onClick={async () => await this.resetForm()}/>}
				       position={'left center'}
				/>
				<Button icon={'save'}
				        color={'green'}
				        content={this.t('save')/*'Save'*/}
				        labelPosition={'right'}
				        loading={this.state.loading}
				        onClick={async () => await this.save()}
				/>
			</ModalActions>
		</Modal>;
	}

	async loading(loading) {
		await this.setState({loading});
	}

	async save() {
		await this.loading(true);
		await this.commit(this.article);
		await this.loading(false);
		await this.modalOpen(false);
	}

	// noinspection JSUnusedLocalSymbols
	onEdited = () => this.props.onEdited || (async value => value);

	async commit(article) {
		await this.api.changeArticle({article});
		await this.onEdited()();
	}

	// noinspection JSMethodCanBeStatic
	renderTitle() {
		return <Header icon={'book'} content={this.t('editing-article')/*'Editing article'*/}/>;
	}

	modalOpen = (open) => async () => await this.setState({open});

	renderBody() {
		// noinspection RequiredAttributes
		return <Form loading={this.state.loading}>
			<Accordion styled fluid>
				<AccordionTitle active={this.state.sections.common}
				                onClick={async () => await this.section('common')}>
					{this.t('common-properties')/*Common properties*/}
				</AccordionTitle>
				<AccordionContent active={this.state.sections.common}>
					<FormInput fluid
					           size={'big'}
					           label={this.t('article-title')/*'Article title'*/}
					           value={this.article.title}
					           placeholder={this.t('example')/*'e.g. Printer fixes'*/}
					           onChange={async ({target}) => await this.editArticle('title', target.value)}
					/>
					<FormDropdown
						fluid
						label={this.t('tags')/*'Tags'*/}
						search
						selection
						multiple
						allowAdditions
						value={this.tags}
						options={this.tagVariants}
						onAddItem={async (evt, {value}) => await this.addTag(value)}
						onChange={async (evt, {value}) => await this.editArticle('tags', value)}
						placeholder={this.t('printer')/*'e.g. printer'*/}
					/>
					<FormField control={TextareaAutosize}
					           value={this.article.brief}
					           onChange={async ({target}) => await this.editArticle('brief', target.value)}
					           placeholder={this.t('brief-article-content')/*'Brief article content'*/}
					           label={this.t('brief-content')/*'Brief content'*/}/>
				</AccordionContent>
				<AccordionTitle active={this.state.sections.code}
				                onClick={async () => await this.section('code')}>
					{this.t('content')/*Content*/}
				</AccordionTitle>
				<AccordionContent active={this.state.sections.code}>
					<Editor
						style={TINYMCE_CONFIG.minHeight}
						init={TINYMCE_CONFIG}
						value={this.article.content}
						onChange={async ({target}) => this.editArticle('content', target.getContent())}/>
				</AccordionContent>
			</Accordion>
		</Form>;
	}

	async section(name) {
		let {sections} = this.state;
		sections[name] = !sections[name];

		await this.setState({sections});
	}

	async editArticle(name, value) {
		let {article} = this.state;
		if (name === 'tags')
			article[name] = _.uniq(value);
		else
			article[name] = value;
		await this.setState({article});
	}

	async addTag(value) {
		let {tags} = this.article;
		tags.push(value);
		await this.editArticle('tags', tags);
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('KnowledgeBase/components/ArticleList/ArticleEditor')(ArticleEditor));