/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React             from 'react';
import ArticleCreator    from './ArticleCreator';
import {Button}          from 'semantic-ui-react';
import Authority         from '../../../api/auth/Authority';
import Authorized        from '../../commons/Authorized';
import {withTranslation} from 'react-i18next';

const AUTHORITY = Authority.anonymous;

class ArticleListControls extends React.Component {
	
	get t() {
		return this.props.t;
	}

	render() {
		return <Authorized authority={AUTHORITY} render>
			<ArticleCreator
				onEdited={async () => await this.updated()}
				trigger={
					<Button
						color={'green'}
						icon={'plus'}
						labelPosition={'right'}
						content={this.t('new-article')/*'New article'*/}
					/>
				}
			/>
		</Authorized>;
	}

	onUpdated = () => this.props.onUpdated || (async value => value);

	async updated() {
		await this.onUpdated()();
	}
}

export default withTranslation('KnowledgeBase/components/ArticleList/ArticleListControls')(ArticleListControls);
