/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React             from 'react';
import _                 from 'lodash';
import {Header}          from 'semantic-ui-react';
import ApiResolver       from '../../../api/ApiResolver';
import '../../../utils/ace-editor';
import '../../../utils/tinymce-editor';
import {ArticleEditor}   from './ArticleEditor';
import moment            from 'moment';
import {connect}         from 'react-redux';
import {redux}           from '../../../utils/misc';
import {withTranslation} from 'react-i18next';
import User              from '../../../api/auth/User';


const DEFAULT_ARTICLE = {
	title: '',
	tags: [],
	published: moment().unix()
};

class ArticleCreator extends ArticleEditor {
	
	get t() {
		return this.props.t;
	}
	
	get api() {
		return ApiResolver.with(this.user, this.toaster).knowledgeBase;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast,
		}
	}

	async resetForm() {
		let article = _.cloneDeep(DEFAULT_ARTICLE);
		await this.setState({article});
	}

	renderTitle() {
		return <Header icon={'book'} content={this.t('new-article')/*'New article'*/}/>;
	}

	async commit(article) {
		await this.api.addArticle({article});
		await this.onEdited()();
	}

}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('KnowledgeBase/components/ArticleList/ArticleCreator')(ArticleCreator));
