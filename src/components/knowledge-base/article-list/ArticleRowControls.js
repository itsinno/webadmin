/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}     from 'react';
import {Button, ButtonGroup} from 'semantic-ui-react';
import ArticleDeleter        from './ArticleDeleter';
import ArticleEditor         from './ArticleEditor';
import Authorized            from '../../commons/Authorized';
import Authority             from '../../../api/auth/Authority';

const CAN_EDIT = Authority.engineer;
const CAN_DELETE = Authority.admin;

export default class ArticleRowControls extends React.Component {
	get article() {
		return this.props.article || {};
	}

	render() {
		return <Fragment>
			<ButtonGroup basic floated={'right'}>
				<ArticleEditor
					article={this.article}
					onEdited={async () => await this.updated()}
					trigger={
						<Authorized prop={'disabled'} authority={CAN_EDIT} on={'deny'}>
							<Button color={'blue'} icon={'edit'}/>
						</Authorized>
					}/>

				<ArticleDeleter
					article={this.article}
					onConfirmed={async () => await this.updated()}
					trigger={
						<Authorized prop={'disabled'} authority={CAN_DELETE} on={'deny'}>
							<Button color={'red'} icon={'trash'}/>
						</Authorized>
					}/>
			</ButtonGroup>
		</Fragment>;
	}

	onUpdate = () => this.props.onUpdate || (async value => value);

	async updated() {
		await this.onUpdate()();
	}
}
