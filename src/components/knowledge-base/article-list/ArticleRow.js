/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                         from 'react';
import {Header, TableCell, TableRow} from 'semantic-ui-react';
import DateTag                       from '../../commons/DateTag';
import Tags                          from '../commons/Tags';
import ArticleRowControls            from './ArticleRowControls';
import {Link}                        from 'react-router-dom';
import Authorized                    from '../../commons/Authorized';
import Authority                     from '../../../api/auth/Authority';

const SHOW_CONTROLS = Authority.engineer;

export default class ArticleRow extends React.Component {

	get article() {
		return this.props.article;
	}

	render() {
		return <TableRow>
			<TableCell selectable>
				<Link to={'/knowledge-base/article/' + this.article.id}>
					<Header size={'small'} content={this.article.title}/>
				</Link>
			</TableCell>
			<TableCell content={this.article.brief}/>
			<TableCell collapsing>
				<DateTag date={this.article.published}/>
			</TableCell>
			<TableCell collapsing>
				<Tags grouped tags={this.article.tags} size={'tiny'}/>
			</TableCell>
			<Authorized name={'ArticleRow'} render authority={SHOW_CONTROLS}>
				<TableCell collapsing>
					<ArticleRowControls article={this.article} onUpdate={async () => await this.updated()}/>
				</TableCell>
			</Authorized>
		</TableRow>;
	}

	onChange = () => this.props.onChange || (async value => value);

	async updated() {
		await this.onChange()();
	}
}
