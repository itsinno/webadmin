/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}   from 'react';
import {Label, LabelGroup} from 'semantic-ui-react';
import _                   from 'lodash';
import {searchUrl}         from './misc';
import {UID}               from '../../../utils/misc';
import {Link}              from 'react-router-dom';

const DEFAULTS = {
	icon: 'tag',
	color: 'green'
};

export default class Tags extends React.Component {
	get tags() {
		return this.props.tags;
	}

	get tagProps() {
		let defaults = _.cloneDeep({...DEFAULTS, ...this.props});
		defaults.tags = undefined;
		defaults.grouped = undefined;
		return defaults;
	}

	render() {
		const tags = this.tags.map(tag => 
			<Label key={UID(tag)} {...this.tagProps} content={tag} as={Link} to={searchUrl(`tag:'${tag}'`)}/>
		);
		if (this.props.grouped)
			return <LabelGroup {...this.tagProps}>{tags}</LabelGroup>;
		return <Fragment>{tags}</Fragment>;
	}
};
