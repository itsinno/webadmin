/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React               from 'react';
import {Loader, Statistic} from 'semantic-ui-react';

class ControlledStatistic extends React.Component {
	get inverted() {
		return this.props.inverted;
	}

	render() {
		return (
			this.props.loading ?
				<Loader inverted={this.inverted} size={'big'} active inline={'centered'}
				        style={{marginBottom: '1em'}}/> :
				<Statistic inverted={this.inverted} size={this.props.size} value={this.props.value || 0}
				           label={this.props.label || ''}/>
		);
	}
}

export default ControlledStatistic;
