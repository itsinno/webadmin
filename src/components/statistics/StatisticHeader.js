/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                 from 'react';
import { Dropdown, Segment } from 'semantic-ui-react';
import { dropdownOption }    from '../../utils/misc';
import { withTranslation }   from 'react-i18next';

const STAT_PERIODS = (t) => [
	dropdownOption('day', t('day')),
	dropdownOption('week', t('week')),
	dropdownOption('month', t('month')),
	dropdownOption('year', t('year'))
];

class StatisticHeader extends React.Component {

	get t() {
		return this.props.t;
	}

	get inverted() {
		return this.props.inverted;
	}

	render() {
		return <Segment inverted={this.inverted} clearing textAlign={'right'}>
			{this.t('show-stats')/*Show statistics for*/}&nbsp;
			<Dropdown
				placeholder='period'
				inline
				value={this.props.period}
				onChange={(e, { value }) => this.onChange()(value)}
				options={STAT_PERIODS(this.t)}
			/>
		</Segment>;
	}

	onChange = () => this.props.onChange || (async value => value);
}

export default withTranslation('Statistics/components/StatisticHeader')(StatisticHeader);
