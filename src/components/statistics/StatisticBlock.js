/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                                                                               from 'react';
import { Grid, GridColumn, GridRow, Header, Label, Segment, SegmentGroup, StatisticGroup } from 'semantic-ui-react';
import { DEFAULT_TYPE }                                                                    from './BlockType';


export default class StatisticBlock extends React.Component {

	get type() {
		return this.props.type || DEFAULT_TYPE;
	}

	get inverted() {
		return this.props.inverted;
	}

	get children() {
		return (this.props.children || [])
			.map((child) => React.cloneElement(
				child,
				{ inverted: this.inverted }
			));
	}

	render() {
		return <SegmentGroup raised className={this.inverted ? 'inverted' : ''}>
			<Segment inverted={this.inverted}>
				<Grid columns={'equal'}>
					<GridRow>
						<GridColumn verticalAlign={'middle'}>
							<Header inverted={this.inverted} size={'tiny'} content={this.props.title}/>
						</GridColumn>
						<GridColumn>
							<Label ribbon={'right'} color={this.type.color}>
								{this.type.text}
							</Label>
						</GridColumn>
					</GridRow>
				</Grid>
			</Segment>
			<Segment padded inverted={this.inverted} textAlign={'center'}>
				<StatisticGroup inverted={this.inverted} widths={this.props.widths} size={this.props.size}>
					{this.children}
				</StatisticGroup>
			</Segment>
		</SegmentGroup>;
	}
}
