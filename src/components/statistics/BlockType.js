const BlockType = {
	STATISTIC: {color: 'violet', text: 'STATISTIC'},
	STATUS: {color: 'green', text: 'STATUS'},
};

export default BlockType;

export const DEFAULT_TYPE = BlockType.STATUS;
