/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                   from 'lodash';
import React               from 'react';
import {
	Grid,
	GridColumn,
	GridRow,
	Header,
	Label,
	Placeholder,
	PlaceholderLine,
	Segment,
	SegmentGroup,
	Table,
	TableBody,
	TableCell,
	TableHeader,
	TableHeaderCell,
	TableRow
}                          from 'semantic-ui-react';
import { DEFAULT_TYPE }    from './BlockType';
import { UID }             from '../../utils/misc';
import { withTranslation } from 'react-i18next';

const RANKS = [
	{ color: 'yellow', icon: { name: 'chess queen' } },
	{ color: 'grey', icon: { name: 'chess queen' } },
	{ color: 'orange', icon: { name: 'chess queen' } },
	{ icon: { name: 'hashtag' } }
];

class RatingBlock extends React.Component {

	get t() {
		return this.props.t;
	}

	get type() {
		return this.props.type || DEFAULT_TYPE;
	}

	get keyName() {
		return this.props.keyName || this.t('name')/*'Name'*/;
	}

	get inverted() {
		return this.props.inverted;
	}

	get valueName() {
		return this.props.valueName || this.t('value')/*'Value'*/;
	}

	get data() {
		const data = _.isArray(this.props.data) ? this.props.data : [];
		return data.slice(0, Math.min(data.length, this.props.count));
	}

	renderRankLabel(rank) {
		let _rank = Math.min(rank, 3);

		let ranks = RANKS;
		ranks[3].color = this.inverted ? 'black' : null;

		return <Label inverted={this.inverted} content={rank + 1} {...ranks[_rank]}/>;
	}

	renderRow(item, rank) {
		return <TableRow key={UID(`row/${rank}`)}>
			<TableCell collapsing content={this.renderRankLabel(rank)}/>
			<TableCell content={item.key}/>
			<TableCell collapsing content={item.value}/>
		</TableRow>;
	}

	renderDummyRow(rank) {
		return <TableRow key={UID(`row/${rank}`)}>
			<TableCell collapsing content={this.renderRankLabel(rank)}/>
			<TableCell>
				<Placeholder><PlaceholderLine fluid/></Placeholder>
			</TableCell>
			<TableCell collapsing>
				<Placeholder><PlaceholderLine fluid/></Placeholder>
			</TableCell>
		</TableRow>;
	}

	renderEmptyRow() {
		return <TableRow key={UID(`row/empty`)}>
			<TableCell colSpan={3}>
				<Header disabled
				        size={'tiny'}
				        textAlign={'center'}
				        className={'no items'}
				        content={this.t('no-items')/*'No items to display'*/}
				/>
			</TableCell>
		</TableRow>;
	}

	render() {
		return <SegmentGroup raised>
			<Segment inverted={this.inverted}>
				<Grid columns={'equal'}>
					<GridRow>
						<GridColumn verticalAlign={'middle'}>
							<Header inverted={this.inverted} size={'tiny'} content={this.props.title}/>
						</GridColumn>
						<GridColumn>
							<Label ribbon={'right'} color={this.type.color}>
								{this.type.text}
							</Label>
						</GridColumn>
					</GridRow>
				</Grid>
			</Segment>
			<Segment inverted={this.inverted} padded textAlign={'center'}>
				<Table celled striped inverted={this.inverted}>
					<TableHeader>
						<TableRow>
							<TableHeaderCell content={this.t('rank')/*'Rank'*/}/>
							<TableHeaderCell content={this.keyName}/>
							<TableHeaderCell content={this.valueName}/>
						</TableRow>
					</TableHeader>
					<TableBody>
						{this.renderTableBody()}
					</TableBody>
				</Table>
			</Segment>
		</SegmentGroup>;
	}

	renderTableBody() {
		if (this.props.loading)
			return _.times(this.props.count, (rank) => this.renderDummyRow(rank));
		else if (this.data.length > 0)
			return this.data.map((value, rank) => this.renderRow(value, rank));
		else
			return this.renderEmptyRow();
	}
}

export default withTranslation('Statistics/components/RatingBlock')(RatingBlock);