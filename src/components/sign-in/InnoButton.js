/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React             from 'react';
import {Button, Image}   from 'semantic-ui-react';
import {connect}         from 'react-redux';
import {redux}           from '../../utils/misc';
import ApiResolver       from '../../api/ApiResolver';
import {withTranslation} from 'react-i18next';

const IMG_SRC =
	'https://university.innopolis.ru/bitrix/templates/.default/static/img/other-images/firsthand-logo.jpg';
const AUTH_TYPE = 'innopolis.sso';

class InnoButton extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			authUrl: '#',
			success: true
		};
	}
	get t() {
		return this.props.t;
	}
	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast
		};
	}

	get api() {
		return ApiResolver.with(null, this.toaster).auth;
	}

	async componentDidMount() {
		await this.loadLinkData();
	}

	async loadLinkData() {
		await this.setState({
			loading: true,
			success: false
		});
		const data = await this.api.getAuthPageURL({
			type: AUTH_TYPE,
			from: this.props.from
		});
		await this.setState({ loading: false });
		if (!data) return;
		await this.setState({
			authUrl: data,
			success: true
		});
	}

	render() {
		return (
			<Button
				fluid
				inverted
				as={'a'}
				color={'green'}
				href={this.state.authUrl}
				loading={this.state.loading}
				disabled={!this.state.success}
			>
				<Image avatar src={IMG_SRC} spaced />
				{this.t('title')}
			</Button>
		);
	}
}

export default connect(
	() => ({}),
	redux.withToaster()
)(withTranslation('SignIn/components/InnoButton')(InnoButton));
