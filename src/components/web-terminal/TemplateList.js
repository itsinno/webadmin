/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                from 'react';
import {ItemListModal}      from '../commons/ItemListModal';
import ApiResolver          from '../../api/ApiResolver';
import TemplateRow          from './template-list/TemplateRow';
import {redux, UID}         from '../../utils/misc';
import TemplateListControls from './template-list/TemplateListControls';
import {connect}            from 'react-redux';
import _                    from 'lodash';
import User                 from '../../api/auth/User';
import { withTranslation } from 'react-i18next';

class TemplateList extends ItemListModal {
	
	get t() {
		return this.props.t;
	}
	
	get strings() {
		return {
			modalHeader: this.t('script-templates')/*'Script templates'*/,
			tableHeaderText: this.t('available-script')/*'Available script templates'*/,
			noItems: this.t('no-templates')/*'No templates available'*/,
			perPage: this.t('page-size')/*'Page size'*/
		};
	}

	get icons() {
		return {
			modalHeader: 'cogs',
			pageResize: 'expand arrows alternate'
		};
	}

	get columnCount() {
		return 3;
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).webTerminal;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast,
		}
	}

	renderHeaderControls() {
		return <TemplateListControls onChange={async () => await this.updateTable()}/>;
	}

	async countItems() {
		return await this.api.countScriptTemplates();
	}

	async loadPage(page, pageSize) {
		return await this.api.getScriptTemplates({page, pageSize});
	}

	renderItemRow(item) {
		return <TemplateRow key={UID('tmpl/' + item.id)}
		                    script={item}
		                    onChange={async () => this.updateTable()}
		                    onExecute={async (command) => await this.executeCommand(command)}/>;
	}

	onExecute = () => this.props.onExecute || ($ => $);

	async executeCommand(value) {
		await this.modalOpen(false);
		await this.onExecute()(value);
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('WebTerminal/components/TemplateList')(TemplateList));