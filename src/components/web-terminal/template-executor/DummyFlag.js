/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React          from 'react';
import {FormCheckbox} from 'semantic-ui-react';

export default class DummyFlag extends React.Component {
	render() {
		return <FormCheckbox className={'flag-dummy'}/>;
	}
}
