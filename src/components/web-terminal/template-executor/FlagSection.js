/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment} from 'react';
import _                 from 'lodash';
import NamedSegment      from '../../commons/NamedSegment';
import Flag              from './Flag';
import {UID}             from '../../../utils/misc';
import FlagGroup         from './FlagGroup';
import DummyFlag         from './DummyFlag';
import Config            from '../../../config/base';
import { withTranslation } from 'react-i18next';

const REGULAR_GROUP_SIZE = Config.webTerminal.templateExecutor.flagSection.regularGroupSize;
const TOGGLE_GROUP_SIZE = Config.webTerminal.templateExecutor.flagSection.toggleGroupSize;

class FlagSection extends React.Component {

	get t() {
		return this.props.t;
	}

	static fillGroup(group) {
		let newGroup = _.clone(group);

		while (newGroup.length < TOGGLE_GROUP_SIZE) {
			newGroup.push(<DummyFlag key={UID('dummy/' + newGroup.length)}/>);
		}

		return newGroup;
	}


	render() {
		let flags = _.cloneDeep(this.props.flags || []);
		let boolFlags = _.remove(flags, _.matches({type: 'boolean'}));

		let groups = this._renderRegularFlagGroups(flags);
		let booleanGroups = this._renderBooleanFlagGroups(boolFlags);

		return <Fragment>
			<NamedSegment title={this.t('script-params')/*'Script params'*/}>{groups}</NamedSegment>
			<NamedSegment title={this.t('script-toggles')/*'Script toggles'*/}>{booleanGroups}</NamedSegment>
		</Fragment>;
	}

	_renderRegularFlagGroups(flags) {
		let flagDOMs = flags.map(flag => this.renderFlag(flag));

		return _.chunk(flagDOMs, REGULAR_GROUP_SIZE).map((group, id) =>
			<FlagGroup key={id}>
				{group}
			</FlagGroup>);
	}

	renderFlag(flag) {
		let value = this.props.value[flag.name];

		return <Flag
			reset={this.props.reset}
			key={UID(flag.name)}
			flag={flag}
			value={value}
			onChange={this._changeFlag(flag.name)}
		/>;
	}

	_renderBooleanFlagGroups(flags) {
		let flagDOMs = flags.map(flag => this.renderFlag(flag));

		return _.chunk(flagDOMs, TOGGLE_GROUP_SIZE).map((group, id) =>
			<FlagGroup key={id}>
				{FlagSection.fillGroup(group)}
			</FlagGroup>);
	}

	onChange = () => this.props.onChange || (() => null);

	_changeFlag = (name) => async (value) => {
		let flags = _.cloneDeep(this.props.value);
		flags[name] = value;

		await this.onChange()(flags);
	};

}

export default withTranslation('WebTerminal/components/TemplateExecutor/FlagSection')(FlagSection);
