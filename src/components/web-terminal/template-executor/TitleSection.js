/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                 from 'lodash';
import React, {Fragment} from 'react';
import {Header}          from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class TitleSection extends React.Component {
	
	get t() {
		return this.props.t;
	}
	
	get title() {
		const prefix = this.props.prefix || this.t('script-template')/*'Script template '*/;

		return <Fragment>{prefix} {this.monoTitle}</Fragment>;
	}

	get monoTitle() {
		let title = this.props.title;

		if (_.isNil(title)) return <Fragment/>;
		return <span className={'monospaced'}>'{title}'</span>;
	}

	render() {
		return <Fragment>
			<Header icon={'cogs'} content={this.title}/>
		</Fragment>;
	}
}

export default withTranslation('WebTerminal/components/TemplateExecutor/TitleSection')(TitleSection);