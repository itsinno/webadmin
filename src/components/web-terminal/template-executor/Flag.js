/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}                                 from 'react';
import {Checkbox, FormCheckbox, FormDropdown, FormInput} from 'semantic-ui-react';
import _                                                 from 'lodash';

const DEFAULT_FLAG = {
	name: 'flag',
	type: 'boolean',
	default: false
};

export default class Flag extends React.Component {
	constructor(props) {
		super(props);
		this.state = this.defaultState;
	}

	get defaultState() {
		return {
			defined: !_.isNil(this.externalValue),
			value: this.externalValue
		};
	}

	get externalValue() {
		return this.props.value;
	}

	get value() {
		return this.state.value;
	}

	get exportedValue() {
		return this.state.defined ? this.state.value : undefined;
	}

	get flag() {
		return this.props.flag || DEFAULT_FLAG;
	}

	async componentDidUpdate(prevProps, prevState, snapshot) {
		if (_.isEqual(prevProps, this.props) && _.isEqual(prevState, this.state)) return;

		if (this.props.reset) {
			await this.setState(this.defaultState);
		}

		if (this.flag.type === 'boolean') await this.setState({defined: true});
	}

	render() {
		let flagDOM = this._renderFlagType();
		return <Fragment>
			{flagDOM}
		</Fragment>;
	}

	_renderFlagType() {
		let flagDOM = <Fragment/>;

		switch (this.flag.type) {
		case 'string':
			flagDOM = this._renderStringFlag();
			break;
		case 'numeric':
			flagDOM = this._renderNumericFlag();
			break;
		case 'boolean':
			flagDOM = this._renderBooleanFlag();
			break;
		case 'enum':
			flagDOM = this._renderEnumFlag();
			break;
		default:
			break;
		}

		return <Fragment>
			{flagDOM}
		</Fragment>;
	}

	_renderNumericFlag() {
		let props = {
			onChange: (evt) => this._setFlagValue(_.toInteger(evt.target.value)),
			label: this._flagDefined(),
			key: this.flag.name,
			type: 'number',
			value: _.toInteger(this.value),
			min: this.flag.range[0],
			max: this.flag.range[1]
		};

		return <Fragment>
			<FormInput {...props} />
		</Fragment>;
	}

	_renderBooleanFlag() {
		let props = {
			onChange: () => this._setFlagValue(!this.value),
			label: this.flag.name,
			key: this.flag.name,
			checked: this.value,
		};
		return <Fragment>
			<FormCheckbox {...props}/>
		</Fragment>;
	}

	_renderEnumFlag() {
		const variants = this.flag.variants.map((value) => ({key: value, value: value, text: value}));
		let props = {
			onChange: (evt, {value}) => this._setFlagValue(value),
			label: this._flagDefined(),
			key: this.flag.name,
			search: true,
			selection: true,
			options: variants,
			value: this.value
		};

		return <Fragment>
			<FormDropdown {...props} />
		</Fragment>;
	}

	_flagDefined() {
		return <Checkbox label={this.flag.name} checked={this.state.defined}
		                 onChange={() => this._toggleDefined()}/>;
	}

	_renderStringFlag() {
		let props = {
			onChange: (evt) => this._setFlagValue(evt.target.value),
			label: this._flagDefined(),
			key: this.flag.name,
			type: 'text',
			value: _.toString(this.value),
			maxLength: this.flag.lengthRange[1]
		};
		return <Fragment>
			<FormInput {...props}/>
		</Fragment>;
	}

	async _setFlagValue(value) {
		await this.setState({defined: true, value});
		await this.onChange()(value);
	}

	onChange = () => this.props.onChange || ($ => $);

	async _toggleDefined() {
		const defined = !this.state.defined;
		await this.setState({defined});
		await this.onChange()(this.exportedValue);
	}
}
