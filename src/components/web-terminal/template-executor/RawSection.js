/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React        from 'react';
import NamedSegment from '../../commons/NamedSegment';
import {FormInput}  from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class RawSection extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			value: ''
		};
	}

	get t() {
		return this.props.t;
	}

	render() {
		let value = this.state.exportedValue || this.props.value || '';
		return <NamedSegment>
			<FormInput
				fluid
				icon={'terminal'}
				iconPosition={'left'}
				label={this.t('raw-args')/*'Raw arguments'*/}
				placeholder={this.t('raw-args')/*'Raw arguments'*/}
				value={value}
				onChange={this._updateArgs()}
			/>
		</NamedSegment>;
	}

	onChange = () => this.props.onChange || (() => null);

	_updateArgs = () => async (evt) => {
		const value = evt.target.value;
		await this.setState({value});
		await this.onChange()(value);
	}
}

export default withTranslation('WebTerminal/components/TemplateExecutor/RawSection')(RawSection);