/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment} from 'react';
import {FormGroup}       from 'semantic-ui-react';

export default class FlagGroup extends React.Component {
	render() {
		const flags = this.props.children || [];

		return <Fragment>
			<FormGroup widths={'equal'} className={'flag-group'}>
				{flags}
			</FormGroup>
		</Fragment>;
	}

}
