/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}                     from 'react';
import {Button, ButtonGroup, Divider, Popup} from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class ButtonSection extends React.Component {
	
	get t() {
		return this.props.t;
	}
	
	render() {
		return <Fragment>
			<ButtonGroup floated={'right'}>
				<Popup trigger={<Button icon={'ban'} onClick={async () => await this._dispatch('reset')}/>}
				       position={'left center'}>
					{this.t('reset-form')/*Reset form*/}
				</Popup>
				<Button content={this.t('exec')/*'Execute'*/} labelPosition={'right'} color={'teal'} icon={'terminal'}
				        onClick={async () => await this._dispatch('executeCommand')}/>
			</ButtonGroup>
			<Divider clearing hidden style={{margin: 0}}/>
		</Fragment>;
	}

	handle = () => this.props.handle || (() => null);

	async _dispatch(eventName) {
		await this.handle()(eventName);
	}
};
export default withTranslation('WebTerminal/components/TemplateExecutor/ButtonSection')(ButtonSection);
