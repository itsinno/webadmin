/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React        from 'react';
import NamedSegment from '../../commons/NamedSegment';
import {FormInput}  from 'semantic-ui-react';
import { withTranslation } from 'react-i18next';

class ResultSection extends React.Component {
	
	get t() {
		return this.props.t;
	}
	
	render() {
		let value = this.props.value || '';
		return <NamedSegment>
			<FormInput
				fluid
				label={this.t('result-command')/*'Resulting command'*/}
				icon={'terminal'}
				iconPosition={'left'}
				placeholder={this.t('result-command')/*'Resulting command'*/}
				className={'input-monospaced'}
				value={value}
				size={'big'}
				onChange={async (value) => await this._updateArgs(value)}
			/>
		</NamedSegment>;
	}

	onChange = () => this.props.onChange || ($ => $);


	async _updateArgs(evt) {
		if (this.props.value === evt.target.value) return;
		await this.onChange()(evt.target.value);
	}
}

export default withTranslation('WebTerminal/components/TemplateExecutor/ResultSection')(ResultSection);