/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React              from 'react';
import {ItemListModal}    from '../commons/ItemListModal';
import ApiResolver        from '../../api/ApiResolver';
import {redux, UID}       from '../../utils/misc';
import ScriptListControls from './script-list/ScriptListControls';
import ScriptRow          from './script-list/ScriptRow';
import {connect}          from 'react-redux';
import _                  from 'lodash';
import User               from '../../api/auth/User';
import { withTranslation } from 'react-i18next';

class ScriptList extends ItemListModal {

	get t() {
		return this.props.t;
	}

	get strings() {
		return {
			modalHeader: this.t('scripts')/*'Scripts'*/,
			tableHeaderText: this.t('available-scripts')/*'Available scripts'*/,
			noItems: this.t('no-scripts')/*'No scripts available'*/,
			perPage: this.t('page-size')/*'Page size'*/
		};
	}

	get icons() {
		return {
			modalHeader: 'cogs',
			pageResize: 'expand arrows alternate'
		};
	}

	get columnCount() {
		return 4;
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).webTerminal;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast,
		}
	}

	renderHeaderControls() {
		return <ScriptListControls onChange={async () => await this.updateTable()}/>;
	}

	async countItems() {
		return await this.api.countScripts();
	}

	async loadPage(page, pageSize) {
		return await this.api.getScripts({page, pageSize});
	}

	renderItemRow(item) {
		return <ScriptRow key={UID('script/' + item.id)}
		                  script={item}
		                  onChange={async () => this.updateTable()}/>;
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('WebTerminal/components/ScriptList')(ScriptList));