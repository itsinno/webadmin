/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                                                     from 'lodash';
import React, {Fragment}                                     from 'react';
import {Button, FormDropdown, FormGroup, FormInput, Segment} from 'semantic-ui-react';
import {dropdownOption}                                      from '../../../utils/misc';
import FlexCol                                               from '../../../utils/components/flex/FlexCol';
import FlexRow                                               from '../../../utils/components/flex/FlexRow';
import { withTranslation } from 'react-i18next';


const DEFAULTS = {
	range: [0, 0],
	lengthRange: [0, 0]
};

class FlagEditor extends React.Component {
	
	
	FLAG_TYPES = [
		{key: 'boolean', value: 'boolean', text: this.t('boolean')/*'Boolean'*/, icon: 'toggle on'},
		{key: 'enum', value: 'enum', text: this.t('enum')/*'Enum'*/, icon: 'th list'},
		{key: 'string', value: 'string', text: this.t('string')/*'String'*/, icon: 'paragraph'},
		{key: 'numeric', value: 'numeric', text: this.t('numeric')/*'Numeric'*/, icon: 'calculator'}
	];
	
	BOOL_DROPDOWN = [
		{key: 'true', value: true, text: this.t('enabled')/*'Enabled'*/, icon: 'toggle on'},
		{key: 'false', value: false, text: this.t('disabled')/*'Disabled'*/, icon: 'toggle off'},
	];

	get t() {
		return this.props.t;
	}
	
	get flag() {
		return this.props.flag || {};
	}

	get enumOptions() {
		return (this.flag.variants || []).map(dropdownOption);
	}

	render() {
		return <Segment>
			<FlexRow alignItems={'center'}>
				<FlexCol grow={1} align={'center'}>
					{this.renderFlagType()}
				</FlexCol>
				<FlexCol grow={0} style={{margin: 0}} align={'center'}>
					<Button inverted
					        color={'red'}
					        icon={'trash'}
					        floated={'right'}
					        onClick={async () => await this.deleteFlag()}
					/>
				</FlexCol>
			</FlexRow>
		</Segment>;
	}

	renderFlagType() {
		switch (this.flag.type) {
		case 'enum':
			return this.renderEnum();
		case 'string':
			return this.renderString();
		case 'numeric':
			return this.renderNumeric();
		case 'boolean':
		default:
			return this.renderBool();
		}
	}

	renderBool() {
		return <Fragment>
			<FormGroup widths={'equal'}>
				<FormInput fluid
				           label={this.t('flag-name')/*'Flag name'*/}
				           value={this.flag.name}
				           className={'input-monospaced'}
				           onChange={async ({target}) => await this.change('name', target.value)}
				/>
				<FormDropdown fluid
				              selection
				              label={this.t('flag-type')/*'Flag type'*/}
				              options={this.FLAG_TYPES}
				              value={this.flag.type}
				              onChange={async (evt, {value}) => await this.change('type', value)}
				/>
				<FormDropdown fluid
				              selection
				              options={this.BOOL_DROPDOWN}
				              label={this.t('default-value')/*'Default value'*/}
				              value={this.flag.default}
				              onChange={async (evt, {value}) => await this.change('default', value)}
				/>
			</FormGroup>
		</Fragment>;
	}

	renderNumeric() {
		return <Fragment>
			<FormGroup widths={'equal'}>
				<FormInput fluid
				           label={this.t('flag-name')/*'Flag name'*/}
				           value={this.flag.name}
				           className={'input-monospaced'}
				           onChange={async ({target}) => await this.change('name', target.value)}
				/>
				<FormDropdown fluid
				              selection
				              label={this.t('flag-type')/*'Flag type'*/}
				              options={this.FLAG_TYPES}
				              value={this.flag.type}
				              onChange={async (evt, {value}) => await this.change('type', value)}
				/>
			</FormGroup>
			<FormGroup widths={'equal'}>
				<FormInput fluid
				           type={'number'}
				           label={this.t('minimal-value')/*'Minimal value'*/}
				           value={this.defaultedValue('range')[0]}
				           max={this.defaultedValue('range')[1]}
				           onChange={async (evt, {value}) => await this.change('range', [value, this.defaultedValue('range')[1]])}
				/>
				<FormInput fluid
				           label={this.t('maximal-value')/*'Maximal value'*/}
				           type={'number'}
				           value={this.defaultedValue('range')[1]}
				           min={this.defaultedValue('range')[0]}
				           onChange={async (evt, {value}) => await this.change('range', [this.defaultedValue('range')[0], value])}
				/>
				<FormInput fluid
				           type={'number'}
				           label={this.t('default-value')/*'Default value'*/}
				           min={this.defaultedValue('range')[0]}
				           max={this.defaultedValue('range')[1]}
				           value={this.flag.default}
				           onChange={async ({target}) => await this.change('default', target.value)}
				/>
			</FormGroup>
		</Fragment>;
	}

	renderString() {
		return <Fragment>
			<FormGroup widths={'equal'}>
				<FormInput fluid
				           label={this.t('flag-name')/*'Flag name'*/}
				           value={this.flag.name}
				           className={'input-monospaced'}
				           onChange={async ({target}) => await this.change('name', target.value)}
				/>
				<FormDropdown fluid
				              selection
				              label={this.t('flag-name')/*'Flag type'*/}
				              options={this.FLAG_TYPES}
				              value={this.flag.type}
				              onChange={async (evt, {value}) => await this.change('type', value)}
				/>

			</FormGroup>
			<FormGroup widths={'equal'}>
				<FormInput fluid
				           label={this.t('default-value')/*'Default value'*/}
				           value={this.flag.default}
				           onChange={async ({target}) => await this.change('default', target.value)}
				/>
				<FormInput fluid
				           type={'number'}
				           label={this.t('minimal-length')/*'Minimal length'*/}
				           value={this.defaultedValue('lengthRange')[0]}
				           max={this.defaultedValue('lengthRange')[1]}
				           onChange={async (evt, {value}) => await this.change('lengthRange', [value, this.defaultedValue('lengthRange')[1]])}
				/>
				<FormInput fluid
				           type={'number'}
				           label={this.t('maximal-length')/*'Maximal length'*/}
				           value={this.defaultedValue('lengthRange')[1]}
				           min={this.defaultedValue('lengthRange')[0]}
				           onChange={async (evt, {value}) => await this.change('lengthRange', [this.defaultedValue('range')[0], value])}/>
			</FormGroup>
		</Fragment>;
	}

	renderEnum() {
		return <Fragment>
			<FormGroup widths={'equal'}>
				<FormInput fluid
				           label={this.t('flag-name')/*'Flag name'*/}
				           value={this.flag.name}
				           className={'input-monospaced'}
				           onChange={async ({target}) => await this.change('name', target.value)}
				/>
				<FormDropdown fluid
				              selection
				              label={this.t('flag-type')/*'Flag type'*/}
				              options={this.FLAG_TYPES}
				              value={this.flag.type}
				              onChange={async (evt, {value}) => await this.change('type', value)}
				/>
				<FormDropdown search selection
				              fluid allowAdditions
				              label={this.t('available-default')/*'Available values/default value'*/}
				              options={this.enumOptions}
				              value={this.flag.default}
				              onChange={async (evt, {value}) => await this.change('default', value)}
				              onAddItem={async (evt, {value}) => await this.enumAddOption(value)}
				/>
			</FormGroup>
		</Fragment>;
	}

	// noinspection JSUnusedLocalSymbols
	onChange = () => this.props.onChange || (async value => value);

	async change(prop, value) {
		let _flag = _.cloneDeep(this.flag);
		_flag[prop] = value;
		return await this.onChange()(_flag);
	}

	async enumAddOption(option) {
		console.log(option);
		let _options = _.clone(this.flag.variants) || [];
		_options.push(option);
		await this.change('variants', _options);
	}

	defaultedValue(name) {
		return this.flag[name] || DEFAULTS[name];
	}

	// noinspection JSUnusedLocalSymbols
	onDelete = () => this.props.onDelete || (async value => value);

	async deleteFlag() {
		await this.onDelete()();
	}
}

export default withTranslation('WebTerminal/components/TemplateEditor/FlagEditor')(FlagEditor);
