/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import { $ConfirmDialog }  from '../../commons/ConfirmDialog';
import ApiResolver         from '../../../api/ApiResolver';
import { redux }           from '../../../utils/misc';
import { connect }         from 'react-redux';
import _                   from 'lodash';
import User                from '../../../api/auth/User';
import { withTranslation } from 'react-i18next';

class TemplateDeleter extends $ConfirmDialog {

	get t() {
		return this.props.t;
	}

	get strings() {
		return {
			modalHeader: this.t('delete-script')/*'Delete script template'*/,
			text: this.t('are-you-sure')/*'This cannot be undone! Are you sure?'*/,
			cancel: this.t('cancel')/*'Cancel'*/,
			confirm: this.t('delete')/*'Delete'*/
		};
	}

	get icons() {
		return {
			modalHeader: 'trash',
			cancel: 'cancel',
			confirm: 'trash'
		};
	}

	async afterConfirm() {
		await this.api.deleteScriptTemplate(this.props.script);
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).webTerminal;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast
		};
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('WebTerminal/components/TemplateList/TemplateDeleter')(TemplateDeleter));