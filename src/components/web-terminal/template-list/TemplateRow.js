/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React                 from 'react';
import {TableCell, TableRow} from 'semantic-ui-react';
import TemplateRowControls   from './TemplateRowControls';
import Config                from '../../../config/base';

const DEFAULT_TEMPLATE = Config.webTerminal.templateList.defaultTemplate;

export default class TemplateRow extends React.Component {
	get script() {
		return this.props.script || DEFAULT_TEMPLATE;
	}

	render() {
		return <TableRow>
			<TableCell content={this.script.command} width={1} className={'monospaced'}/>
			<TableCell content={this.script.description}/>
			<TableCell collapsing>
				<TemplateRowControls script={this.props.script}
				                     onChange={async () => await this.changed()}
				                     onExecute={async (command) => await this.executeCommand(command)}/>
			</TableCell>
		</TableRow>;
	}

	onChange = () => this.props.onChange || (async value => value);
	onExecute = () => this.props.onExecute || ($ => $);

	async changed() {
		await this.onChange()();
	}

	async executeCommand(value) {
		await this.onExecute()(value);
	}
}
