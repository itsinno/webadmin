/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}     from 'react';
import {Button, ButtonGroup} from 'semantic-ui-react';
import TemplateExecutor      from './TemplateExecutor';
import TemplateDeleter       from './TemplateDeleter';
import TemplateEditor        from './TemplateEditor';

export default class TemplateRowControls extends React.Component {
	render() {
		return <Fragment>
			<ButtonGroup basic floated={'right'}>
				<TemplateExecutor script={this.props.script}
				                  onExecute={async (command) => await this.executeCommand(command)}
				                  trigger={<Button icon={'play circle'} color={'teal'}/>}/>
				<TemplateEditor script={this.props.script}
				                onEdited={async () => await this.templateChanged()}
				                trigger={<Button icon={'edit'} color={'blue'}/>}/>
				<TemplateDeleter script={this.props.script}
				                 onConfirmed={async () => await this.templateChanged()}
				                 trigger={<Button icon={'trash'} color={'red'}/>}
				/>
			</ButtonGroup>
		</Fragment>;
	}

	onExecute = () => this.props.onExecute || ($ => $);
	onChange = () => this.props.onChange || ($ => $);

	async executeCommand(value) {
		await this.onExecute()(value);
	}

	async templateChanged() {
		await this.onChange()();
	}
}
