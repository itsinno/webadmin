/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                from 'lodash';
import React            from 'react';
import {
	Button,
	Form,
	FormField,
	FormGroup,
	FormInput,
	Modal,
	ModalActions,
	ModalContent,
	Popup,
	Segment,
	SegmentGroup
}                       from 'semantic-ui-react';
import TitleSection     from '../template-executor/TitleSection';
import NamedSegment     from '../../commons/NamedSegment';
import TextareaAutosize from 'react-textarea-autosize';
import FlagEditor       from '../template-editor/FlagEditor';
import ApiResolver      from '../../../api/ApiResolver';
import {redux, UID}     from '../../../utils/misc';
import {connect}        from 'react-redux';
import User             from '../../../api/auth/User';
import { withTranslation } from 'react-i18next';

const DEFAULT_FLAG = {
	type: 'string',
	name: ''
};

export class TemplateEditor extends React.Component {


	constructor(props) {
		super(props);
		this.state = {
			originalName: '',
			script: {},
			open: false,
			loading: false
		};
	}

	
	get t() {
		return this.props.t;
	}

	get script() {
		return this.state.script;
	}

	get flags() {
		return this.script.flags || [];
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).webTerminal;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast,
		}
	}

	async componentDidMount() {
		await this.resetForm();
	}

	async resetForm() {
		let {script} = _.cloneDeep(this.props);
		await this.setState({script, originalName: script.command});
	}

	render() {
		return <Modal open={this.state.open}
		              trigger={this.props.trigger}
		              onOpen={this.modalOpen(true)}
		              onClose={this.modalOpen(false)}>
			{this.renderTitle()}
			<ModalContent scrolling>
				{this.renderBody()}
			</ModalContent>
			<ModalActions>
				<Popup content={this.t('reset-form')/*'Reset form'*/}
				       disabled={this.state.loading}
				       trigger={<Button icon={'ban'} onClick={async () => await this.resetForm()}/>}
				       position={'left center'}
				/>
				<Button icon={'save'}
				        color={'green'}
				        content={this.t('save')/*'Save'*/}
				        labelPosition={'right'}
				        loading={this.state.loading}
				        onClick={async () => await this.save()}
				/>
			</ModalActions>
		</Modal>;
	}

	async loading(loading) {
		await this.setState({loading});
	}

	async save() {
		await this.loading(true);
		await this.commit(this.state.script);
		await this.loading(false);
		await this.modalOpen(false);
	}

	// noinspection JSUnusedLocalSymbols
	onEdited = () => this.props.onEdited || (async value => value);

	async commit(script) {
		await this.api.changeScriptTemplate({script});
		await this.onEdited()();
	}

	renderTitle() {
		return <TitleSection title={this.state.originalName}/>;
	}

	modalOpen = (open) => async () => await this.setState({open});

	renderBody() {
		return <Form loading={this.state.loading}>
			<SegmentGroup>
				<NamedSegment title={this.t('common-properties')/*'Common properties'*/}>
					<FormGroup>
						<FormInput fluid width={13}
						           value={this.script.command}
						           onChange={async ({target}) => await this.editScript('command', target.value)}
						           label={this.t('base-command')/*'Base command'*/}
						           className={'input-monospaced'}
						           placeholder={this.t('server-upgrade')/*'e.g. server-upgrade'*/}/>
						<FormInput width={3}
						           value={this.script.flagSeparator}
						           onChange={async ({target}) => await this.editScript('flagSeparator', target.value)}
						           label={this.t('flag-separator')/*'Flag separator'*/}
						           className={'input-monospaced'}
						           placeholder={this.t('equal')/*'e.g. ='*/}/>
					</FormGroup>
					<FormField control={TextareaAutosize}
					           value={this.script.description}
					           onChange={async ({target}) => await this.editScript('description', target.value)}
					           placeholder={this.t('brief-template-description')/*'Brief template description'*/}
					           label={this.t('description')/*'Description'*/}/>
				</NamedSegment>
				<NamedSegment title={this.t('flags-configuration')/*'Flags configuration'*/}>
					<SegmentGroup>
						{this.flags.map((flag, id) =>
							<FlagEditor key={UID('flag/' + id)}
							            flag={flag}
							            onDelete={async () => this.deleteFlag(id)}
							            onChange={async (value) => this.changeFlag(id, value)}
							/>
						)}
						<Segment>
							<Button icon={'plus square'}
							        content={this.t('add-flag')/*'Add flag'*/}
							        labelPosition={'left'}
							        onClick={async () => await this.addFlag()}
							/>
						</Segment>
					</SegmentGroup>
				</NamedSegment>
			</SegmentGroup>
		</Form>;
	}

	async editScript(name, value) {
		let {script} = this.state;
		script[name] = value;
		await this.setState({script});
	}

	async addFlag() {
		let {script} = this.state;
		script.flags.push(DEFAULT_FLAG);
		await this.setState({script});
	}

	async changeFlag(id, value) {
		let {script} = this.state;
		script.flags[id] = value;
		await this.setState({script});
	}

	async deleteFlag(id) {
		let {script} = this.state;
		script.flags = _.remove(script.flags, (_, idx) => {
			return id !== idx;
		});
		await this.setState({script});
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('WebTerminal/components/TemplateList/TemplateEditor')(TemplateEditor));