/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment} from 'react';
import {Button}          from 'semantic-ui-react';
import TemplateCreator   from './TemplateCreator';
import { withTranslation } from 'react-i18next';

class TemplateListControls extends React.Component {
	
	get t() {
		return this.props.t;
	}
	
	render() {
		return <Fragment>
			<TemplateCreator
				onCreated={async () => await this.templateListChanged()}
				trigger={
					<Button content={this.t('new')/*'New'*/}
					        color={'green'}
					        floated={'right'}
					        icon={'plus square'}
					        labelPosition={'right'}
					/>
				}/>
		</Fragment>;
	}

	onListChanged = () => this.props.onListChanged || (async value => value);

	async templateListChanged() {
		await this.onListChanged()();
	}
}

export default withTranslation('WebTerminal/components/TemplateList/TemplateListControls')(TemplateListControls);