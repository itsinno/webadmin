/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                   from 'lodash';
import React               from 'react';
import { withTranslation } from 'react-i18next';
import { connect }         from 'react-redux';
import ApiResolver         from '../../../api/ApiResolver';
import User                from '../../../api/auth/User';
import { redux }           from '../../../utils/misc';
import TitleSection        from '../template-executor/TitleSection';
import { TemplateEditor }  from './TemplateEditor';

const DEFAULT_TEMPLATE = {
	command: '',
	description: '',
	flagJoin: '',
	args: true,
	flags: []
};

class TemplateCreator extends TemplateEditor {
	constructor(props) {
		super(props);
		this.state.script = DEFAULT_TEMPLATE;
	}

	get t() {
		return this.props.t;
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).webTerminal;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast
		};
	}

	async resetForm() {
		await this.setState({ script: DEFAULT_TEMPLATE });
	}

	render() {
		return super.render();
	}

	async commit(script) {
		await this.api.addScriptTemplate({ script });
		await this.onCreated()();
	}

	renderTitle() {
		return <TitleSection prefix={this.t('create-script')/*'Create script'*/}/>;
	}

	onCreated = () => this.props.onCreated || (async value => value);
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('WebTerminal/components/TemplateList/TemplateCreator')(TemplateCreator));