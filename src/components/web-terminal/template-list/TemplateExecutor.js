/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                                                       from 'lodash';
import React                                                   from 'react';
import {Form, Modal, ModalActions, ModalContent, SegmentGroup} from 'semantic-ui-react';
import FlagSection                                             from '../template-executor/FlagSection';
import TitleSection                                            from '../template-executor/TitleSection';
import RawSection                                              from '../template-executor/RawSection';
import ButtonSection                                           from '../template-executor/ButtonSection';
import ResultSection                                           from '../template-executor/ResultSection';
import ScriptParser                                            from '../../../utils/web-terminal/ScriptParser';

class TemplateExecutor extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			values: {
				flags: {},
				args: ''
			},
			command: '',
			loading: false,
			reset: false,
			open: false
		};
	}

	get defaultState() {
		let values = {
			flags: this._flagDefaults(),
			args: ''
		};
		return {
			values,
			command: this._parsedCommand(values),
			loading: false
		};
	}

	async componentDidUpdate(prevProps, prevState, snapshot) {
		if (_.isEqual(prevState, this.state) && this.state.reset) {
			await this.setState({reset: false});
		}
		if (!this.state.reset) return;
		await this.setState(this.defaultState);
	}

	async componentDidMount() {
		console.info('mount');
		await this.setState(this.defaultState);
	}

	render() {
		return <Modal open={this.state.open}

		              trigger={this.props.trigger}
		              onOpen={this.modalOpen(true)}
		              onClose={this.modalOpen(false)}>
			<TitleSection title={this.props.script.command}/>
			<ModalContent>
				{this.renderBody()}
			</ModalContent>
			<ModalActions>
				<ButtonSection handle={async (eventName) => await this._handle(eventName)}/>
			</ModalActions>
		</Modal>;
	}

	modalOpen = (open) => async () => await this.setState({open});

	renderBody() {
		return <Form loading={this.state.loading}>
			<SegmentGroup className={'attached both'}>
				<FlagSection reset={this.state.reset} flags={this.props.script.flags} value={this.state.values.flags}
				             onChange={this.updateValues('flags')}/>
				<RawSection value={this.state.values.args}
				            onChange={async (value) => await this.updateValues('args')(value)}/>
				<ResultSection value={this.state.command} onChange={async (value) => this._changeCommand(value)}/>
			</SegmentGroup>
		</Form>;
	}

	updateValues = (name) => async (value) => {
		let values = this.state.values;
		values[name] = value;

		await this.setState({values, command: this._parsedCommand()});
	};

	_parsedCommand(values = undefined) {
		let _values = values || this.state.values;
		const parser = new ScriptParser(this.props.script);
		return parser.compile(_values);
	}

	_flagDefaults() {
		let flags = _.get(this.props, 'script.flags', []);
		return _.fromPairs(flags.map(flag => [flag.name, flag.default]));
	}

	async _changeCommand(command) {
		await this.setState({command});
	}

	async _handle(eventName) {
		switch (eventName) {
		case 'reset':
			return await this.resetForm();
		case 'executeCommand':
			return await this.executeCommand(this.state.command);
		default:
			return;
		}
	}

	async resetForm() {
		await this.setState({reset: true});
	}

	onExecute = () => this.props.onExecute || ($ => $);

	async executeCommand(value) {
		await this.modalOpen(false)();
		await this.onExecute()(value);
	}
}

export default TemplateExecutor;
