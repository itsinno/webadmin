/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React            from 'react';
import _                from 'lodash';
import {
	Accordion,
	AccordionContent,
	AccordionTitle,
	Button,
	Form,
	FormDropdown,
	FormField,
	FormGroup,
	FormInput,
	Modal,
	ModalActions,
	ModalContent,
	Popup
}                       from 'semantic-ui-react';
import ApiResolver      from '../../../api/ApiResolver';
import TitleSection     from '../template-executor/TitleSection';
import TextareaAutosize from 'react-textarea-autosize';
import Config           from '../../../config/base';
import AceEditor        from 'react-ace';
import '../../../utils/ace-editor';
import {redux}          from '../../../utils/misc';
import {connect}        from 'react-redux';
import User             from '../../../api/auth/User';
import { withTranslation } from 'react-i18next';

const LANGS = Config.webTerminal.scriptList.supportedLangs;
const ACE_CONFIG = Config.webTerminal.scriptEditor.aceConfig;

const LANG_OPTIONS = _.map(LANGS, (value, key) => ({
	key,
	value: key,
	icon: value.icon,
	text: value.name
}));

export class ScriptEditor extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			originalName: '',
			script: {},
			open: false,
			loading: false,
			sections: {
				common: true,
				code: true
			}
		};
	}

	get t() {
		return this.props.t;
	}

	get script() {
		return this.state.script;
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).webTerminal;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast,
		}
	}

	async componentDidMount() {
		await this.resetForm();
	}

	async resetForm() {
		let {script} = _.cloneDeep(this.props);
		await this.setState({script, originalName: script.filename});
	}

	render() {
		return <Modal open={this.state.open}
		              trigger={this.props.trigger}
		              onOpen={this.modalOpen(true)}
		              onClose={this.modalOpen(false)}>
			{this.renderTitle()}
			<ModalContent>
				{this.renderBody()}
			</ModalContent>
			<ModalActions>
				<Popup content={this.t('reset-form')/*'Reset form'*/}
				       disabled={this.state.loading}
				       trigger={<Button icon={'ban'} onClick={async () => await this.resetForm()}/>}
				       position={'left center'}
				/>
				<Button icon={'save'}
				        color={'green'}
				        content={this.t('save')/*'Save'*/}
				        labelPosition={'right'}
				        loading={this.state.loading}
				        onClick={async () => await this.save()}
				/>
			</ModalActions>
		</Modal>;
	}

	async loading(loading) {
		await this.setState({loading});
	}

	async save() {
		await this.loading(true);
		await this.commit(this.state.script);
		await this.loading(false);
		await this.modalOpen(false);
	}

	// noinspection JSUnusedLocalSymbols
	onEdited = () => this.props.onEdited || (async value => value);

	async commit(script) {
		await this.api.changeScript({script});
		await this.onEdited()();
	}

	renderTitle() {
		return <TitleSection prefix={this.t('script')/*'Script'*/} title={this.state.originalName}/>;
	}

	modalOpen = (open) => async () => await this.setState({open});

	renderBody() {
		return <Form loading={this.state.loading}>
			<Accordion styled fluid>
				<AccordionTitle active={this.state.sections.common}
				                onClick={async () => await this.section('common')}>
					{this.t('common-properties')/*Common properties*/}
				</AccordionTitle>
				<AccordionContent active={this.state.sections.common}>
					<FormGroup widths={'equal'}>
						<FormInput value={this.script.filename}
						           onChange={async ({target}) => await this.editScript('filename', target.value)}
						           label={this.t('filename')/*'Filename'*/}
						           className={'input-monospaced'}
						           placeholder={this.t('server-upgrade')/*'e.g. server-upgrade.rb'*/}/>
						<FormDropdown label={this.t('language')/*'Language'*/}
						              search selection
						              value={this.script.lang}
						              options={LANG_OPTIONS}
						              onChange={async (evt, {value}) => await this.editScript('lang', value)}
						              placeholder={this.t('ruby')/*'e.g. Ruby'*/}/>
					</FormGroup>
					<FormField control={TextareaAutosize}
					           value={this.script.description}
					           onChange={async ({target}) => await this.editScript('description', target.value)}
					           placeholder={this.t('brief-template-description')/*'Brief template description'*/}
					           label={this.t('description')/*'Description'*/}/>
				</AccordionContent>
				<AccordionTitle active={this.state.sections.code}
				                onClick={async () => await this.section('code')}>
					{this.t('source-code')/*Source Code*/}
				</AccordionTitle>
				<AccordionContent active={this.state.sections.code}>
					<AceEditor
						value={this.script.code}
						width={'100%'}
						height={'400px'}
						onChange={async (value) => await this.editScript('code', value)}
						mode={this.script.lang}
						theme={'one-light'}
						name={'code-editor'}
						setOptions={ACE_CONFIG}
					/>
				</AccordionContent>
			</Accordion>
		</Form>;
	}

	async section(name) {
		let {sections} = this.state;
		sections[name] = !sections[name];

		await this.setState({sections});
	}

	async editScript(name, value) {
		let {script} = this.state;
		script[name] = value;
		await this.setState({script});
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('WebTerminal/components/ScriptList/ScriptEditor')(ScriptEditor));