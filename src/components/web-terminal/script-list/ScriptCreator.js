/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import _                   from 'lodash';
import React               from 'react';
import { withTranslation } from 'react-i18next';
import { connect }         from 'react-redux';
import ApiResolver         from '../../../api/ApiResolver';
import User                from '../../../api/auth/User';
import Config              from '../../../config/base';
import '../../../utils/ace-editor';
import { redux }           from '../../../utils/misc';
import TitleSection        from '../template-executor/TitleSection';
import { ScriptEditor }    from './ScriptEditor';

const DEFAULT_SCRIPT = Config.webTerminal.scriptList.defaultScript;

class ScriptCreator extends ScriptEditor {

	constructor(props) {
		super(props);
		this.state = {
			script: _.cloneDeep(DEFAULT_SCRIPT),
			open: false,
			loading: false,
			sections: {
				common: true,
				code: true
			}
		};
	}

	// noinspection JSUnusedLocalSymbols
	onEdited = () => this.props.onEdited || (async value => value);

	get t() {
		return this.props.t;
	}

	get api() {
		return ApiResolver.with(this.user, this.toaster).webTerminal;
	}

	get user() {
		return _.get(this.props, 'auth.user', User.anonymous);
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast
		};
	}

	async resetForm() {
		await this.setState({ script: _.cloneDeep(DEFAULT_SCRIPT), foo: 'bar' });
	}

	async commit(script) {
		await this.api.addScript({ script });
		await this.onEdited()();
	}

	renderTitle() {
		return <TitleSection prefix={this.t('new-script')/*'New script'*/}/>;
	}
}

export default connect(
	redux.with('auth'),
	redux.withToaster()
)(withTranslation('WebTerminal/components/ScriptList/ScriptCreator')(ScriptCreator));