/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}     from 'react';
import {Button, ButtonGroup} from 'semantic-ui-react';
import ScriptDeleter         from './ScriptDeleter';
import ScriptEditor          from './ScriptEditor';

export default class ScriptRowControls extends React.Component {
	render() {
		return <Fragment>
			<ButtonGroup basic floated={'right'}>
				<ScriptEditor script={this.props.script}
				              onEdited={async () => await this.scriptChanged()}
				              trigger={<Button icon={'edit'} color={'blue'}/>}
				/>
				<ScriptDeleter script={this.props.script}
				               trigger={<Button icon={'trash'} color={'red'}/>}
				               onConfirmed={async () => await this.scriptChanged()}
				/>
			</ButtonGroup>
		</Fragment>;
	}

	onChange = () => this.props.onChange || ($ => $);

	async scriptChanged() {
		await this.onChange()();
	}
}
