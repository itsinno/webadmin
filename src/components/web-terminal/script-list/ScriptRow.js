/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, { Fragment }           from 'react';
import { Icon, TableCell, TableRow } from 'semantic-ui-react';
import ScriptRowControls             from './ScriptRowControls';
import Config                        from '../../../config/base';

const DEFAULT_SCRIPT = Config.webTerminal.scriptList.defaultScript;
const LANGS = Config.webTerminal.scriptList.supportedLangs;

export default class ScriptRow extends React.Component {
	get script() {
		return this.props.script || DEFAULT_SCRIPT;
	}

	get lang() {
		return LANGS[this.props.script.lang] || LANGS.unknown;
	}

	renderLang() {
		return <Fragment><Icon fitted {...this.lang.icon}/> {this.lang.name}</Fragment>;
	}

	render() {
		return <TableRow>
			<TableCell content={this.script.filename} width={1} className={'monospaced'}/>
			<TableCell content={this.renderLang()} collapsing className={'monospaced'}/>
			<TableCell content={this.script.description}/>
			<TableCell collapsing>
				<ScriptRowControls script={this.props.script}
				                   onChange={async () => await this.changed()}
				                   onExecute={async (command) => await this.executeCommand(command)}/>
			</TableCell>
		</TableRow>;
	}

	onChange = () => this.props.onChange || (async value => value);
	onExecute = () => this.props.onExecute || ($ => $);

	async changed() {
		await this.onChange()();
	}

	async executeCommand(value) {
		await this.onExecute()(value);
	}
}
