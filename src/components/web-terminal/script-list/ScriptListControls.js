/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React         from 'react';
import {Button}      from 'semantic-ui-react';
import ScriptCreator from './ScriptCreator';
import { withTranslation } from 'react-i18next';

class ScriptListControls extends React.Component {
	
	get t() {
		return this.props.t;
	}
	
	render() {
		return <ScriptCreator
			onCreated={async () => await this.scriptListChanged()}
			trigger={
				<Button content={this.t('new')/*'New'*/}
				        color={'green'}
				        floated={'right'}
				        icon={'plus square'}
				        labelPosition={'right'}
				/>
			}/>;
	}

	onListChanged = () => this.props.onListChanged || (async value => value);

	async scriptListChanged() {
		await this.onListChanged()();
	}
}

export default withTranslation('WebTerminal/components/ScriptList/ScriptListControls')(ScriptListControls);