/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import i18n               from 'i18next';
import {initReactI18next} from 'react-i18next';
import Config             from './config/base';
import resources          from './locales';

console.log(resources);

export const I18NEXT_CONFIG = {
	// backend: {
	// 	loadPath: "/locales/{{lng}}/{{ns}}.json",
	// 	ajax: function (url, options, cb, data) {
	// 		url = url.replace(/&#x2F;/g, "/");
	// 		axios.get(url, {
	// 				transformResponse: data => data
	// 			})
	// 			.then(result => cb(result.data, result))
	// 			.catch(error => cb('{}', {status: 200}));
	// 	}
	// },
	resources,
	fallbackLng: Config.general.defaultLanguage,
	whitelist: Config.general.languages,
	debug: true
};

i18n
	.use(initReactI18next)
	.init(I18NEXT_CONFIG);

export default i18n;
