/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, {Fragment}                     from 'react';
import {Container}                           from 'semantic-ui-react';
import KnowledgeBase                         from './pages/KnowledgeBase';
import Article                               from './pages/knowledge-base/Article';
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';
import WebTerminal                           from './pages/WebTerminal';
import Toaster                               from './utils/components/toaster/Toaster';
import {connect}                             from 'react-redux';
import AppBar                                from './components/commons/AppBar';
import {buildLocation, parseSearch, redux}   from './utils/misc';
import SignIn                                from './pages/SignIn';
import NotFound                              from './pages/NotFound';
import Statistics                            from './pages/Statistics';
import Bots                                  from './pages/Bots';
import AccessManager                         from './pages/AccessManager';
import Authorize                             from './pages/Authorize';
import Authorized                            from './components/commons/Authorized';
import Authority                             from './api/auth/Authority';
import LanguageSelector                      from './components/commons/LanguageSelector';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {};
	}

	get toasts() {
		return this.props.toasts || [];
	}

	get toaster() {
		return {
			openToast: this.props.openToast,
			closeToast: this.props.closeToast
		};
	}

	get here() {
		const {location} = this.props;
		return buildLocation(location || {pathname: '/'});
	}

	static renderArticle(id) {
		return <Article id={id}/>;
	}

	static renderKBSearcher(location) {
		let params = parseSearch(location.search);
		return <KnowledgeBase query={params.q}/>;
	}

	async componentDidMount() {
	}

	render() {
		return (
			<Fragment>
				<Switch>
					<Route path={'/authorize'} component={Authorize}/>
					<Route path={'/sign-in'} component={SignIn}/>
					<Route
						path={'/'}
						render={(...params) => this.renderMain(...params)}
					/>
				</Switch>
				<Toaster
					toasts={this.toasts}
					onToastClosed={async id => await this.closeToast(id)}
				/>
				<LanguageSelector/>
			</Fragment>
		);
	}

	renderMain() {
		return (
			<Fragment>
				<AppBar/>
				<div className={'marginPanel'}/>
				<Container>
					<Authorized render on={'deny'} exact authority={Authority.anonymous}>
						{this.renderRouter()}
					</Authorized>
					<Authorized render on={'allow'} exact authority={Authority.anonymous}>
						<Redirect
							to={{
								pathname: '/sign-in',
								search: `?from=${this.here}`
							}}
						/>
					</Authorized>
				</Container>
			</Fragment>
		);
	}

	renderRouter() {
		return (
			<Switch>
				<Route path={'/bots'} component={Bots}/>

				<Route path={'/terminal'} component={WebTerminal}/>

				<Route
					path={'/knowledge-base/article/:id'}
					render={({match}) => App.renderArticle(match.params.id)}
				/>
				<Route
					path={'/knowledge-base/search'}
					render={({location}) => App.renderKBSearcher(location)}
				/>
				<Route path={'/knowledge-base'} component={KnowledgeBase}/>

				<Route path={'/stats'} component={Statistics}/>

				<Route path={'/access-manager'} component={AccessManager}/>

				<Route path={'/users'} component={AccessManager}/>

				<Route exact path={'/'} render={() => <Redirect to={'/stats'} />} />

				<Route component={NotFound}/>
			</Switch>
		);
	}

	async closeToast(id) {
		await this.props.closeToast({id});
	}
}

export default connect(
	redux.with('toasts'),
	redux.withToaster()
)(withRouter(App));
