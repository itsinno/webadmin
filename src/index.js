/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import React, { Suspense }                from 'react';
import ReactDOM                           from 'react-dom';
import { Provider }                       from 'react-redux';
import { BrowserRouter }                  from 'react-router-dom';
import { PersistGate }                    from 'redux-persist/integration/react';
import 'sui-theme-webadmin/semantic.min.css';
import { Dimmer, DimmerDimmable, Loader } from 'semantic-ui-react';
import App                                from './App';
import './App.css';
import './i18n';
import './index.css';
import { persistor, store }               from './redux/store';
import * as serviceWorker                 from './serviceWorker';

const Fallback = () => <DimmerDimmable dimmed={true}>
	<Dimmer active={true}>
		<Loader/>
	</Dimmer>
</DimmerDimmable>;

ReactDOM.render(
	<Suspense fallback={<Fallback/>}>
		<Provider store={store}>
			<PersistGate persistor={persistor} loading={<Dimmer active><Loader/></Dimmer>}>
				<BrowserRouter>
					<App/>
				</BrowserRouter>
			</PersistGate>
		</Provider>
	</Suspense>,
	document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
