/*
 * Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

const SOURCE_EXTENSION = '.lc.yml';

const fs = require('fs');
const path = require('path');
const readdir = require('recursive-readdir');
const yaml = require('js-yaml');
const rimraf = require('rimraf');

const cutExtension = (file, extension = null) => {
	extension = extension || path.extname(file);
	return file.replace(extension, '');
};

const loadFile = (inputFile) => {
	let data = yaml.safeLoad(fs.readFileSync(inputFile));

	if (data.$inherit) {
		const inheritedFile = path.resolve(
			path.dirname(inputFile),
			data.$inherit + SOURCE_EXTENSION
		);

		data = {
			...loadFile(inheritedFile),
			...data
		};

		delete data.$inherit;
	}

	return data;
};
const saveFile = (outputFile, data) => fs.writeFileSync(outputFile, JSON.stringify(data, null, '\t'));

const buildDir = async (inputDir, outputFile) => {
	console.log('\'%s\':\n  to: \'%s\'', inputDir, outputFile);
	console.log('  files:');

	if (!fs.lstatSync(inputDir).isDirectory()) {
		throw new Error('Invalid input directory');
	}

	outputFile = path.resolve(outputFile);
	if (fs.existsSync(outputFile) && !fs.lstatSync(outputFile).isFile()) {
		throw new Error('Destination exists and is not a file');
	}

	if (fs.existsSync(outputFile))
		rimraf.sync(outputFile);

	const files = (await readdir(inputDir))
		.filter((file) => fs.lstatSync(file).isFile())
		.filter((file) => file.endsWith(SOURCE_EXTENSION))
		.map((file) => [(file), cutExtension(file, SOURCE_EXTENSION)]);

	const object = {};

	for (let [inputFile, outputFile] of files) {
		let parts = path.relative(inputDir, outputFile).split(path.sep);
		const lang = parts.shift();
		const namespace = parts.join(path.sep);

		console.log('    - %s', inputFile);

		object[lang] = object[lang] || {};
		object[lang][namespace] = loadFile(path.resolve(inputFile));
	}

	saveFile(path.resolve(outputFile), object);
};

console.log('---');

const args = process.argv.slice(2, 4);

buildDir(...args)
	.then($ => $)
	.catch(e => {
		throw e;
	});
