const fs = require('fs');
const path = require('path');

const strfmt = (string) => (args) => {
    for(const key in args) {
        const rx = new RegExp(`\\%\\{${key}\\}`);
        string = string.replace(rx, args[key]);
    }

    return string;
};

const processFile = (from, to, env) => {
    console.log('  %s: %s', from, to);
    
    from = path.resolve(from);
    to = path.resolve(to);
    
    const data = fs.readFileSync(from).toString();
	const processed = strfmt(data)(env);

	fs.writeFileSync(to, processed);

	return true;
};

const [, , input, output] = process.argv;

try {
    console.log('---');
    console.log('file: ');
	processFile(input, output, process.env);
} catch (e) {
	console.error('  error: %s', e.message);
}
