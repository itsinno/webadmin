# Translation guide

## Commons
 - Available languages are defined in `src/config/base.js:Config.general.languages`.
 - Default language can be set by `src/config/base.js:Config.general.defaultLanguage`.
 - Languages are named by two-letter code, e.g. `ru`, `en` or `de`.
 - Language files are YAML files stored in `locales/{language name}/`.
## Namespaces
 * Each namespace has its own language file, named `{namespace id}.lc.yml`
 * Namespace ids can contain slashes, e.q. `SignIn/components/InnoButton`
   * In this case, namespace file must be stored in subdirectory (e.g. `SignIn/components/InnoButton.lc.yml`).
 * Each React component must have its own namespace.
   * Page component's namespace must be placed in root folder (e.g. for `SignIn` page it will be `locales/{language name}/SignIn`)
   * Helper component's (stored in `src/components`) namespace must be placed under its supercomponent directory.
     * Page subcomponent's (supercomponent is a page) namespace must be placed under `{page}/components` directory.
     * Common component's (stored in `src/components/common`) namespace must be under `commons` directory.
     * Helper subcomponent's (supercomponent is helper component) namespace must be placed under `{path to component}/` directory (e.g. for `src/components/bots/bot-list/BotRow.js` it will be `Bots/components/BotList/BotRow.lc.yml`).
   * Utility component's (stored in `src/utils/components/{group}`) namespace must be placed under `externals/{group}/` directory (e.g. for `src/utils/components/toaster/Toaster.js` it will be `externals/toaster/Toaster.lc.yml`).
   * Any other component's namespace must be placed under `misc/`.
## Building translation bundle

Translation bundle can be built using `yarn locales:build` command.
Also, for `yarn start` and `yarn build` translation bundle will be generated automatically.
To reload translation bundle while `yarn start` is running, rebuild bundle by `yarn locales:build` and refresh page in browser.