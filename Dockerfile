# Copyright (C) 2019 Dmitriy O. 'dussolo' Ussoltsev - All rights reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential

# Double-stage container build, used to minimize container size
FROM node:carbon-alpine AS build

WORKDIR /app

# Copying package.json first to enforce node_modules caching
COPY package.json /app
COPY yarn.lock /app

RUN yarn install

# Copying files separately to enforce caching
COPY ./public /app/public
COPY ./tools /app/tools
COPY ./craco.config.js /app/craco.config.js
COPY ./src /app/src
COPY ./locales /app/locales

# loading arguments
ARG WEBADMIN_ROOT=https://helpdesk.innopolis.university
ARG GENERAL_API_ROOT=$WEBADMIN_ROOT/api/
ARG AUTH_API_ROOT=$GENERAL_API_ROOT/auth/
ARG BOTS_API_ROOT=$GENERAL_API_ROOT/bots/

ENV WEBADMIN_ROOT ${WEBADMIN_ROOT}
ENV GENERAL_API_ROOT ${GENERAL_API_ROOT}
ENV AUTH_API_ROOT ${AUTH_API_ROOT}
ENV BOTS_API_ROOT ${BOTS_API_ROOT}

RUN yarn run configs:generate
RUN yarn run build

FROM sebp/lighttpd AS prod

# Removing target directory to omit conflicts
RUN rm -rf /var/www/localhost/htdocs

# Creating required directories
RUN mkdir -p /var/lib/lighttpd/cache/compress
RUN chown -R lighttpd:lighttpd /var/lib/lighttpd/cache

COPY --from=build /app/build/ /var/www/localhost/htdocs

# Notice that *.conf have been never copied before
# So if only *.conf changed, all previous steps are taken from cache
COPY conf/lighttpd.conf /etc/lighttpd/lighttpd.conf

EXPOSE 80
